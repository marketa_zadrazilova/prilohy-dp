const ActionTypes = {
	/* --- LOG IN ------------------------------------------------------------ */
	LOG_IN_REQUEST: 'LOG_IN_REQUEST',
	LOG_IN_SUCCESS: 'LOG_IN_SUCCESS',
	LOG_IN_FAILURE: 'LOG_IN_FAILURE',

	LOG_OUT: 'LOG_OUT',

	/* --- COMMENTS ---------------------------------------------------------- */
	COMMENTS_FETCH_REQUEST: 'COMMENTS_FETCH_REQUEST',
	COMMENTS_FETCH_SUCCESS: 'COMMENTS_FETCH_SUCCESS',
	COMMENTS_FETCH_FAILURE: 'COMMENTS_FETCH_FAILURE',
};

export default ActionTypes;