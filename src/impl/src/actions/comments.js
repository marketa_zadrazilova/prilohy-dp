import ActionTypes from './ActionTypes';
import * as api from '../fakeit/ApiCalls';


function fetchSucceed(comments) {
	return {
		type: ActionTypes.COMMENTS_FETCH_SUCCESS,
		comments
	};
}

function fetchFailed(errorMessage) {
	return {
		type: ActionTypes.COMMENTS_FETCH_FAILURE,
		errorMessage
	};
}

export function fetchComments(projectId) {
	return dispatch => {

		return api.getComments().then(comments => {
				dispatch(fetchSucceed(comments));
			}).catch(errorMessage => {
				dispatch(fetchFailed(errorMessage));
			});
	};
}

// add comment + retreive list of comments
export function addComment(projectId, comment) {
	return dispatch => {

		return api.createComment(projectId, comment).then(response => {
				dispatch(fetchComments(projectId));
			}).catch(errorMessage => {

			});
	};
}