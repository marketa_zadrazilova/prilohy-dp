export const categories = [
    {label: 'Hry a soutěže', value: 'games'},
    {label: 'Sport', value: 'sport'},
    {label: 'Komunita a setkávání', value: 'community'},
    {label: 'Kultura a umění', value: 'art-culture'},
    {label: 'Hudba a tanec', value: 'music'},
    {label: 'Pro děti', value: 'children'},
    {label: 'Kurzy, přednášky a vzdělávání', value: 'education'},
    {label: 'Kultivace veřejného prostoru', value: 'public-space'},
    {label: 'Ostatní', value: 'other'}
];

export const getCategoryLabel = (value) => {
    return categories.find(category => category.value === value).label;
}

export const getCategoryValue = (label) => {
    return categories.find(category => category.label === label).value;
}