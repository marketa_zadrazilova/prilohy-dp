export const projectStates = {
    'NEW': 1,                   //Koncept
    'AWAITING': 2,              //Kontrola obsahu
    'STUDY': 3,                 //Studie realizovatelnosti
    'FEASIBLE': 4,              //Realizovatelny
    'IN_FUNDING': 5,            //Sbirka
    'IN_REALIZATION': 6,        //Uspesny
    'UNSUCCESSFULL': 7,         //Neuspesny
    'CANCELLED': 8              //Zruseny
};

export const PRIVATE_STATES = ['NEW', 'AWAITING', 'CANCELLED'];
export const PUBLIC_STATES = ['STUDY', 'FEASIBLE', 'IN_FUNDING', 'IN_REALIZATION','UNSUCCESSFULL'];

const states = {
    'NEW': {
        label: 'Koncept'
    },
    'AWAITING': {
        label: 'Kontrola obsahu'
    },
    'STUDY': {
        label: 'Studie realizovatelnosti'
    },
    'FEASIBLE': {
        label: 'Realizovatelný'   
    },
    'IN_FUNDING': {
        label: 'Sbírka'
    },
    'IN_REALIZATION': {
        label: 'Úspěšný'
    },
    'UNSUCCESSFULL': {
        label: 'Neúspěšný'
    },
    'CANCELLED': {
        label: 'Zrušený'
    }
};

export const getStateLabel = key => {
    if (! states.hasOwnProperty(key)) throw new Error(`Unknown state '${key}'.`);

    return states[key].label;
};
