import React from 'react';
import PropTypes from 'prop-types';
import { Col, Panel, Row } from 'react-bootstrap';
import { Form, FormControl, ControlLabel, FormGroup } from 'react-bootstrap';
import { categories } from '../constants/Categories';


export default function FilterBar({onFilter, onSort}) {
	const filterOptions = categories.map(category => <option value={category.value}>{category.label}</option>)

	return (
		<Row>
			<Col md={12}>
				<Panel className="filter-bar">
					<Form inline>
						Filtrovat

					    <FormGroup controlId="filterByType">
							<ControlLabel>dle kategorie</ControlLabel>
							<FormControl 
								componentClass="select" 
								placeholder="select"
								onChange={e => onFilter('type', e.target.value)}
							>
								<option value="ALL">všechny</option>
								{filterOptions}
							</FormControl>
						</FormGroup>

					    <FormGroup controlId="filterByState">
							<ControlLabel>dle stavu</ControlLabel>
							<FormControl 
								componentClass="select" 
								placeholder="select"
								onChange={e => onFilter('state', e.target.value)}
							>
								<option value="ALL">všechny</option>
								<option value="STUDY">Studie proveditelnosti</option>
								<option value="FEASIBLE">Realizovatelný</option>
								<option value="IN_FUNDING">Sbírka</option>
								<option value="IN_REALIZATION">V realizaci</option>
							</FormControl>
						</FormGroup>

					    <FormGroup className="pull-right" controlId="sortBy">
							<ControlLabel>Řadit</ControlLabel>
							<FormControl 
								componentClass="select" 
								placeholder="select"
								onChange={onSort}
							>
								<option value="nameAsc">dle názvu A-Z</option>
								<option value="nameDesc">dle názvu Z-A</option>
							</FormControl>
						</FormGroup>
					</Form>
				</Panel>
			</Col>
		</Row>
	);
}

FilterBar.propTypes = {
	onFilter: PropTypes.func.isRequired,
	onSort: PropTypes.func.isRequired,
}