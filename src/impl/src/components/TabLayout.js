import React from 'react';


export default class TabLayout extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			activeTab: 1
		}
	}

	handleSelect(key) {
		const activeTab = Number(key);

		this.setState({
			activeTab
		});
	}

	onPrev() {
		const newTab = this.state.activeTab - 1;

		this.setState({
			activeTab: (newTab < 1) ? 1 : newTab,
		});
	}

	onNext() {
		const newTab = this.state.activeTab + 1;

		this.setState({
			activeTab: newTab > 5 ? 5 : newTab,
		});
	}

	render() {
		const navItems = {};
		const tabPanes = {};

		<Tab.Container 
				activeKey={activeTab} 
				id="new-project"
				onSelect={handleSelect}
			>
				<Row className="clearfix">
					<Col md={3}>
						<Nav bsStyle="pills" stacked>
							{navItems}
						</Nav>
					</Col>
					<Col md={9}>
						<Tab.Content>
							{tabPanes}
						</Tab.Content>
					</Col>
				</Row>
		</Tab.Container>
	}
}	