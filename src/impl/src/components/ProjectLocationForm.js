import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import SetLocationMap from './maps/SetLocationMap';
import LabeledInput from './forms/LabeledInput';


export default function ProjectLocationForm({
	isEditable,
	location,
	onLocationChange
}) {
	return (
		<Row className="location-pane">
			<Col sm={12}>
				<SetLocationMap 
					location={location}
					onLocationChange={onLocationChange}
					isEditable={isEditable}
				/>
			</Col>
			<Col sm={12}>
				<LabeledInput 
					id="location-info"
					label="Upřesnění místa"
					type="text"
					name="title"
					value={location.title}
					onChange={onLocationChange}
					disabled={!isEditable}
				/>
			</Col>
		</Row>
	);
}


ProjectLocationForm.defaultProps = {
	isEditable: true
}

ProjectLocationForm.propTypes = {
	isEditable: PropTypes.bool,
	location: PropTypes.shape({
		title: PropTypes.string,
		coordinateLat: PropTypes.number,
		coordinateLong: PropTypes.number,
	}).isRequired,
	onLocationChange: PropTypes.func.isRequired,
}