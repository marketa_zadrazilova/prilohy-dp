import React from 'react';
import { Button, Col,  ControlLabel, InputGroup, Form, FormControl, FormGroup, Panel, Row, Table } from 'react-bootstrap';
import PaymentMethodsForm from './ProjectDetail/PaymentMethodsForm';


export default function Contributions(props) {
	return (
		<Row>
			<Col md={4}>
				<Panel>
					<h4>Přispět finančně</h4>
					<Form>
						<FormGroup controlId="">
							<ControlLabel>Částka </ControlLabel>
							<InputGroup>
								<FormControl type="number"/>
								<InputGroup.Addon>Kč</InputGroup.Addon>
							</InputGroup>
						</FormGroup>

						<br />
						<PaymentMethodsForm />

						<br />
						<Button>Přejít k platbě</Button>
					</Form>
				</Panel>
			</Col>
			<Col md={4}>
				<Panel>
					<h4>Přihlásit se jako dobrovolník</h4>

					<form>
						<FormGroup controlId="">
							<ControlLabel>S čím mohu pomoci?</ControlLabel>
							<FormControl componentClass="textarea" /> 
						</FormGroup>
						<Button>Přihlásit se jako dobrovolník</Button>
					</form>
				</Panel>
			</Col>
			<Col md={4}>
				<Panel>
					<h4>Přispět materiálním darem</h4>
					<Form>

						<Table>
							<thead>
								<tr>
									<th>Název</th>
									<th>Množství</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Pisek</td>
									<td>
										<FormControl type="number"/>
									</td>
									<td>kg</td>
								</tr>
								<tr>
									<td>Pisek</td>
									<td>
										<FormControl type="number"/>
									</td>
									<td>kg</td>
								</tr>
							</tbody>
						</Table>

						<FormGroup controlId="">
							<ControlLabel>Poznámka</ControlLabel>
							<FormControl componentClass="textarea" /> 
						</FormGroup>

						<Button>Přislíbit materiál</Button>
					</Form>
				</Panel>
			</Col>
		</Row>
	);
}