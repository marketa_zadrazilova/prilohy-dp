/* eslint-disable no-undef */

import React from 'react';


export default class GoogleMap extends React.Component {

	componentDidMount() {
		let self = this;

		// Asynchronously load the Google Maps script, passing in the callback reference
		loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyDTkF_if4sbuJR0hwa_NGY5DMMdlYZhMUE', () => {
			console.log('ready to render', self);
			self.map = new google.maps.Map(self.refs.map, { 
	          center: {lat: 49.293861, lng: 14.158865},
	          scrollwheel: false,
	          zoom: 8
	        });

	        self.map.addListener('click', (e) => {

	        	var marker = new google.maps.Marker({
	        		position: e.latLng,
	        		map: self.map
	        	});

	        });
		});
	}

	render() {
		const mapStyle = {
			width: 400,
			height: 400
		};

		return (
			<div ref='map' style={mapStyle}>Here should be a map</div>
		);
	}
}

GoogleMap.propTypes = {
	//coordinates: React.PropTypes.object.isRequired
};

function loadJS(src, callback) {
	var ref = window.document.getElementsByTagName("script")[0];
	var script = window.document.createElement("script");
	script.src = src;
	script.async = true;

    script.onreadystatechange = callback;
    script.onload = callback;

	ref.parentNode.insertBefore(script, ref);
}

/*
	http://www.klaasnotfound.com/2016/11/06/making-google-maps-work-with-react/
*/