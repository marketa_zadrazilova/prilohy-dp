import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, FormControl, FormGroup } from 'react-bootstrap';


export default class CommentForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			value: ''
		};

		this.onValueChange = this.onValueChange.bind(this);
		this.onFormSubmitted = this.onFormSubmitted.bind(this);
	}

	onValueChange(e) {
		e.preventDefault();

		this.setState({
			value: e.target.value
		});
	}

	onFormSubmitted(e) {
		e.preventDefault();

		this.props.onFormSubmitted(this.state.value);

		this.setState({
			value: ''
		});
	}

	render() {
		return (
			<Form className="comment-form">
				<FormGroup controlId="formControlsTextarea">
					<FormControl 
						componentClass="textarea" 
						placeholder="Vložte komentář ..." 
						value={this.state.value}
						onChange={this.onValueChange}
					/>
				</FormGroup>

				<Button 
					type="submit" 
					onClick={this.onFormSubmitted}
					disabled={this.state.value.trim() === ''}
				>
					Odeslat
				</Button>
			</Form>
		);
	}
}

CommentForm.propTypes = {
	onFormSubmitted: PropTypes.func.isRequired
};