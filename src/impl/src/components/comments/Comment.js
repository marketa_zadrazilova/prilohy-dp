import React from 'react';
import PropTypes from 'prop-types';
import { Col, Image, Row } from 'react-bootstrap';
import DateFormatter from '../common/DateFormatter';


export default function Comment(props) {
	const {
		author,
		created_at,
		text
	} = props;

	return (
		<Row className="comment">
				<Col md={1} className="comment-img">
					<Image src={author.image_url} circle responsive/>
				</Col>
				<Col md={11}>
					<p className="comment-header">
						<span><strong><a href={author.link}>{author.name}</a></strong></span>
						<span className="pull-right text-muted"><DateFormatter date={created_at} /></span>
					</p>
					<p>{text}</p>
				</Col>
		</Row>
	);
}

Comment.propTypes = {
	author: PropTypes.object.isRequired,
	created_at: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
};