import React from 'react';
import PropTypes from 'prop-types';
import Comment from './Comment';
import CommentForm from './CommentForm';
import InfoBox from '../common/InfoBox';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './comments.css';
import SignUpModal from '../modals/SignUpModal';

/**
 * TODO: řadit komentáře od nejnovějších po nejstarší
 * TODO: pagination
 */
export default function Comments(props) {
	const {
		isAuthenticated
	} = props.auth;

	var comments;

	if (props.comments.length > 0) {
		const sortedComments = props.comments.sort((a,b) => {
			return (a.created_at < b.created_at) ? 1 : ((a.created_at > b.created_at) ? -	1 : 0); 
		});

		comments = sortedComments.map((comment) => {
			return (
				<Comment 
					author={comment.author} 
					created_at={comment.created_at}
					key={comment.id}
					text={comment.text} 
				/>
			);
		});
	} else {
		comments = <p className="text-muted">Ještě nebyly přidány žádné komentáře.</p>
	}

	return (
		<Row>
			<Col md={10}>
				<h3>Komentáře</h3>

				{isAuthenticated ? (
					<CommentForm onFormSubmitted={props.onFormSubmitted} />
				) : (
					<InfoBox 
						title="Komentáře mohou přidávat pouze přihlášení uživatelé."
					>
						<p>
							Pokud se chcete přidat do diskuse, 
							<Link to='/prihlaseni'> přihlaste se</Link>, 
							nebo se <Link to="/registrace">zaregistrujte</Link>.
						</p>
					</InfoBox>
				)}
				
				{ comments }
			</Col>
		</Row>
	);
}

Comments.propTypes = {
	comments: PropTypes.array.isRequired,
	onFormSubmitted: PropTypes.func.isRequired,
};