import React from 'react';
import PropTypes from 'prop-types';
import {Col, Row} from 'react-bootstrap';


const style = {
	margin: '10px 0'
};

export default function Pagination({
	firstIndex,
	lastIndex,
	total,
	onPrevious,
	onNext,
}) {
	return (
		<Row style={style}>
			<Col md={4} className="text-left">
				{firstIndex > 1 &&
					<a onClick={onPrevious}>&lt; Předchozí</a>
				}
			</Col>
			<Col md={4} className="text-center">
				Zobrazeno {firstIndex}-{lastIndex} z {total}
			</Col>
			<Col md={4} className="text-right">
				{lastIndex < total &&
					<a onClick={onNext}>Následující &gt;</a>
				}
			</Col>
		</Row>
	);
}

Pagination.propTypes = {
	firstIndex: PropTypes.number.isRequired,
	lastIndex: PropTypes.number.isRequired,
	total: PropTypes.number.isRequired,
	onPrevious: PropTypes.func,
	onNext: PropTypes.func,
};