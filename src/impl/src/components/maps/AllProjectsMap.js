/* eslint-disable no-undef */
import React from 'react';


const mapStyle = {
	width: '100%',
	height: 340,
	backgroundColor: '#f7f7f7',
	border: '1px solid #e3e3e3',
};

const projects = [
	{
		title: 'Piáno v parku',
		link: '/projekty/1',
		location: {lat: 49.309181, lng: 14.143456}		
	},
	{
		title: 'Obnova návěštidla',
		link: '/projekty/2',
		location: {lat: 49.309770, lng: 14.148525}		
	},
	{
		title: 'Komunitní zahrádka',
		link: '/projekty/3',
		location: {lat: 49.309191, lng: 14.241525},	
	},
	{
		title: 'Šachové odpoledne',
		link: '/projekty/4',
        location: {lat: 49.308358, lng: 14.148363}
    },
    {
		title: 'Sousedské slavnosti',
		link: '/projekty/5',
        location: {lat: 49.302746, lng: 14.147731}
    },
    {
		title: 'Malování na chodník',
		link: '/projekty/6',
        location: {lat: 49.309219, lng: 14.170142}
    },
];

export default class AllProjectsMap extends React.Component {
	constructor(props) {
		super(props);

		this.loadMap = this.loadMap.bind(this);
	}


	//TODO: InfoWindow content as ReactComponent, https://github.com/tomchentw/react-google-maps/issues/25
	loadMap() {
		const map = new google.maps.Map(this.refs.map, {
			zoom: 12,
			center: {lat: 49.301521, lng: 14.1899071}
		});

		var infoWindow = new google.maps.InfoWindow();
		var marker = null;
		var markers = [];
		
		for (var project of projects) {
			marker = new google.maps.Marker({
				position: project.location,
				map: map
			});

			marker.addListener('click', ((marker, project) => {
				return () => {
					var content = "<div><a href='" + project.link + "'>" + project.title + "</a></div>";
					infoWindow.setContent(content);
					infoWindow.open(map, marker);
				};
			})(marker, project));

			markers.push(marker);
		}
	}

	componentDidMount() {
		this.loadMap();
	}

	render() {
		return (
			<div id='map' ref='map' style={mapStyle} />
		);
	}
}