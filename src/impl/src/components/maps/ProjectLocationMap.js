/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';


const mapStyle = {
	width: '100%',
	height: 200,
	backgroundColor: '#f7f7f7',
	border: '1px solid #e3e3e3',
};

export default class ProjectLocationMap extends React.Component {
	constructor(props) {
		super(props);

		this.initMap = this.initMap.bind(this);
	}

	initMap() {
		const {
			coordinateLat,
			coordinateLong
		} = this.props.location;

		const map = new google.maps.Map(this.refs.map, {
			zoom: 17,
			center: {
				lat: coordinateLat,
				lng: coordinateLong
			}
		});

		new google.maps.Marker({
			position: {
				lat: coordinateLat,
				lng: coordinateLong
			},
			map: map
		});
	}

	componentDidMount() {
		this.initMap();
	}

	render() {
		return (
			<div>
				<p>{this.props.location.title}</p>
				<div id="map" ref="map" style={mapStyle}>Map placeholder</div>
			</div>
		);
	}
}

ProjectLocationMap.propTypes = {
	location: PropTypes.shape({
		coordinateLat: PropTypes.number.isRequired,
		coordinateLong: PropTypes.number.isRequired,
		title: PropTypes.string,
	}).isRequired
};