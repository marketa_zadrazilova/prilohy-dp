/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';


const mapStyle = {
	width: '100%',
	height: 350,
	margin: '15px 0',
};

export default class SetLocationMap extends React.Component {
	constructor(props) {
		super(props);

		this.loadMap = this.loadMap.bind(this);
	}

	loadMap() {
		const map = new google.maps.Map(this.refs.map, {
			zoom: 17,
			center: {
				lat: 49.309713,
				lng: 14.149001
			}
		});

		const marker = new google.maps.Marker({
			draggable: true
		});

		
		marker.addListener('rightclick', (e) => {
			marker.setMap(null);
		});

		map.addListener('click', (e) => {
			console.log(e.latLng);
			this.positionChanged(e.latLng);
			marker.setPosition(e.latLng);
			marker.setMap(map);
		});
	}

	positionChanged(newPosition) {
		console.log(newPosition.lat());
		console.log(newPosition.lng());
	}

	componentDidUpdate(prevProps, prevState) {
		this.loadMap();
	}

	render() {
		return (
			<div id='map' ref='map' style={mapStyle} />
		);
	}
}

SetLocationMap.propTypes = {
	onLocationChanged: PropTypes.func.isRequired,
};