import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'react-bootstrap';
import NewsForm from '../news/NewsForm';

export default function AddNews({
	showModal,
	onShowModal,
	onHideModal,
	...props
}) {
	return (
		<div>
			<Button
				onClick={onShowModal}
			>
				Přidat novinku
			</Button>

			<Modal show={showModal} onHide={onHideModal}>
				<Modal.Header closeButton>
					<Modal.Title>Přidat novinku</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<NewsForm 
						form={props.form}
						onFormChange={props.onFormChange}
						onFormSubmit={props.onFormSubmit}
					/>
				</Modal.Body>
			</Modal>
		</div>
	);
}

AddNews.propTypes = {
	form: PropTypes.object.isRequired,
	onFormChange: PropTypes.func.isRequired,
	onFormSubmit: PropTypes.func.isRequired,
	showModal: PropTypes.bool.isRequired,
	onHideModal: PropTypes.func.isRequired,
	onShowModal: PropTypes.func.isRequired,
}