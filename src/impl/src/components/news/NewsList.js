import React from 'react';
import PropTypes from 'prop-types';
import News from './News';

import './news.css';


function sortByDateDESC(firstDate, secondDate) {
	if (firstDate < secondDate) {
		return 1;
	} else if (firstDate === secondDate) {
		return 0;
	} 

	return -1;
}

export default function NewsList({news}) {
	const newsList = news.map((item) => <News news={item} key={item.id}/>);
	const sortedList = newsList.sort((a,b) => sortByDateDESC(a.created_at, b.created_at));

	return (
		<div className='newsList'>	
			{ newsList.length > 0 ? (
					sortedList
				): (
					<p className='text-muted'>Ještě nebyly vloženy žádné novinky.</p>
				)
			}
		</div>
	);
}

NewsList.propTypes = {
	news: PropTypes.array.isRequired,
};
