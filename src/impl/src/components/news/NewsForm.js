import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import LabeledInput from '../forms/LabeledInput';
import LabeledTextarea from '../forms/LabeledTextarea';

import './news.css';


export default function NewsForm({
	form,
	onFormChange,
	onFormSubmit,
}) {
	return (
		<div className='news-form'>
			<form>
				<LabeledInput 
					id="news-title"
					type="text"
					label="Titulek"
					name="title"
					value={form.title}
					onChange={onFormChange}
				/>
				<LabeledTextarea 
					id="news-text"
					label="Text"
					name="text"
					value={form.text}
					onChange={onFormChange}
				/>
				<Button onClick={onFormSubmit}>Zveřejnit</Button>
			</form>
		</div>
	);
}

NewsForm.propTypes = {
	form: PropTypes.shape({
		title: PropTypes.string,
		text: PropTypes.string,
	}).isRequired,
	onFormChange: PropTypes.func.isRequired,
	onFormSubmit: PropTypes.func.isRequired
}