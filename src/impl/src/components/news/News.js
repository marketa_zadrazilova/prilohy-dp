import React from 'react';
import PropTypes from 'prop-types';
import DateFormatter from '../common/DateFormatter';

import './news.css';


export default function News(props) {
	const {
		title,
		text,
		created_at
	} = props.news;

	return (
		<div className='news'>
			<p className='title'>{title}</p>
			<p className='date text-muted'><DateFormatter date={created_at} /></p>
			<p>
				{text}
			</p>
		</div>
	);
}

News.propTypes = {
	news: PropTypes.shape({
		title: PropTypes.string.isRequired,
		text: PropTypes.string.isRequired,
		created_at: PropTypes.string.isRequired,
	})
};