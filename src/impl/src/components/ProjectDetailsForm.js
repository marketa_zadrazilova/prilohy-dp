import React from 'react';
import PropTypes from 'prop-types';
import { Form, Row } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import FieldGroup from './forms/FieldGroup';
import LabeledInput from './forms/LabeledInput';
import LabeledTextarea from './forms/LabeledTextarea';
import { categories as categoriesOptions } from '../constants/Categories';


export default function ProjectDetailsForm({
	isEditable,
	projectDetails,
	onItemChange,
	onCategoriesChange
}) {
	return (
		<div>
			<Row>
				<Form className="col-md-6">
					<LabeledInput
						id="new-project-title"
						isRequired={true}
						type="text"
						label="Název projektu"
						name="name"
						value={projectDetails.name}
						onChange={onItemChange}
						disabled={!isEditable}
					/>

					<FieldGroup 
						id="formControlsSelect" 
						isRequired={true}
						label="Kategorie projektu"
					>
						<Select 
							name="categories"
							options={categoriesOptions}
							placeholder="Zvolte kategorie projektu"
							multi
							disabled={!isEditable}
							value={projectDetails.categories}
							onChange={onCategoriesChange}
							closeOnSelect={false}
							simpleValue
							searchable={false}
						/>
					</FieldGroup>

					<LabeledTextarea
						id="new-project-short"
						isRequired={true}
						label="Krátký popis"
						name="descriptionShort"
						value={projectDetails.descriptionShort}
						onChange={onItemChange}
						disabled={!isEditable}
					/>

					<LabeledTextarea
						id="new-project-descr"
						isRequired={true}
						label="Delší popis"
						name="descriptionLong"
						value={projectDetails.descriptionLong}
						onChange={onItemChange}
						disabled={!isEditable}
					/>
				</Form>
			</Row>
		</div>
	);
}

ProjectDetailsForm.defaultProps = {
	isEditable: true,
}

ProjectDetailsForm.propTypes = {
	isEditable: PropTypes.bool,
	projectDetails: PropTypes.shape({
		name: PropTypes.string.isRequired,
		categories: PropTypes.array.isRequired,
		descriptionShort: PropTypes.string.isRequired,
		descriptionLong: PropTypes.string.isRequired,
	}).isRequired,
	onItemChange: PropTypes.func.isRequired,
};