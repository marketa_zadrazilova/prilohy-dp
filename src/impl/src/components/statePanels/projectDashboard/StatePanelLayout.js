import React from 'react';
import PropTypes from 'prop-types';
import { Col, Panel, Row } from 'react-bootstrap';
import ActionButtons from './ActionButtons';


export default function StatePanelLayout({
    title,
    description,
    project
}) {
    return (
        <Row>
            <Col md={9}>
                <Panel>
                    <h4>{title}</h4>
                    <p>
                        {description}
                    </p>
                </Panel>
            </Col>
            <Col md={3}>
                <ActionButtons state={project.projectState} />
            </Col>
        </Row>
    );
}

StatePanelLayout.props = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    project: PropTypes.object.isRequired
};