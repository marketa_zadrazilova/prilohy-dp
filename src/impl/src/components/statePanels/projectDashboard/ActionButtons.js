import React from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';
import CancelProjectConfirmation from '../../modals/CancelProjectConfirmation';

const PRIVATE_PROJECTS = ['AWAITING', 'NEW'];

export default function ActionButtons(props) {
    return (
        <div>
            { PRIVATE_PROJECTS.includes(props.state) ? (
                <Button 
                    style = {{marginBottom: '5px'}}
                    block
                >
                    <Glyphicon glyph="search" /> Zobrazit náhled projektu
                </Button>
            ) : (
                    <Button 
                    style = {{marginBottom: '5px'}}
                    block
                >
                    <Glyphicon glyph="search" /> Zobrazit veřejnou stránku projektu
                </Button>
            )
            }
            <CancelProjectConfirmation />
        </div>
    )
}

ActionButtons.props = {
    state: PropTypes.string.isRequired
}