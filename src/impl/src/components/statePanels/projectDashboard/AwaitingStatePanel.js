import React from 'react';
import StatePanelLayout from './StatePanelLayout';


export default function AwaitingStatePanel(props) {
	return (
		<StatePanelLayout
			title="Projekt čeká na schválení"
			description="Projekt nebude zveřejněn dokud nebude chválen. 
			V tuto chváli nelze projekt upravovat."
			project={props.project}
		/>
	);
}