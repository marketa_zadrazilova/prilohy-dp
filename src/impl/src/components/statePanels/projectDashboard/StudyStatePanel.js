import React from 'react';
import PropTypes from 'prop-types';
import StatePanelLayout from './StatePanelLayout';

export default function StudyStatePanel({project}) {
    return (
        <StatePanelLayout
            title="Právě probíhá studie realizovatelnosti"
            description="Projekt čeká na zpětnou vazbu od zástupců jednotlivých odborů.
            Projekt je veřejně přístupný. Můžete začít sbírat zpětnou vazbu."
            project={project}
        />
    );
}

StudyStatePanel.propTypes = {
    project: PropTypes.object.isRequired
};