import React from 'react';
import PropTypes from 'prop-types';
import StatePanelLayout from './StatePanelLayout';


export default function FeasibleStatePanel(props) {
    return (
        <StatePanelLayout
            title="Projekt je realizovatelný"
            description="Gratulujeme, Váš projekt byl vyhodnocen jako realizovatelný. V tuto chvíli můžete spustit sbírku."
            project={props.project}
        />
    );
}

FeasibleStatePanel.props = {
    project: PropTypes.shape({
        projectState: PropTypes.string.isRequired
    }).isRequired
};