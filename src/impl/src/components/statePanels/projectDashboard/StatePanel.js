import React from 'react';

import NewStatePanel from './NewStatePanel';
import AwaitingStatePanel from './AwaitingStatePanel';
import StudyStatePanel from './StudyStatePanel';
import FeasibleStatePanel from './FeasibleStatePanel';
import FundingStatePanel from './FundingStatePanel';

export default function StatePanel({project}) {
	switch(project.projectState) {
		case 'NEW':
			return <NewStatePanel project={project}/>;
		case 'AWAITING':
			return <AwaitingStatePanel project={project}/>;
		case 'STUDY':
			return <StudyStatePanel project={project} />;
		case 'FEASIBLE':
			return <FeasibleStatePanel project={project} />;
		case 'IN_FUNDING':
			return <FundingStatePanel project={project} />;
		default:
			return null;
	}
}