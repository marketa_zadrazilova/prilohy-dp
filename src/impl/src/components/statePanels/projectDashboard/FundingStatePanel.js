import React from 'react';
import PropTypes from 'prop-types';
import StatePanelLayout from './StatePanelLayout';


export default function FundingStatePanel({project}) {
	return (
		<StatePanelLayout
			title="Projekt je ve fázi sbírka"
			description=""
			project={project}
		/>
	);
}

FundingStatePanel.propTypes = {
	project: PropTypes.object.isRequired,
};	