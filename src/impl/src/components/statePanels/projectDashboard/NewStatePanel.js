import React from 'react';
import StatePanelLayout from './StatePanelLayout';


export default function NewStatePanel(props) {
	return (
		<StatePanelLayout
			title="Projekt je uložen jako koncept"
			description="V tuto chvíli vidíte projekt pouze vy. Pro zveřejnění projektu je potřeba dokončit formuář a poslat
			projekt ke schválení pomocí tlačítka Vytvořit projekt, kterou najdete v záložce Dokončit projekt."
			project={props.project}
		/>
	);
}