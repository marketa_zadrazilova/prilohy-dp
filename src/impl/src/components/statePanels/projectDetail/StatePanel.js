import React from 'react';
import StudyStatePanel from './StudyStatePanel';
import FeasibleStatePanel from './FeasibleStatePanel';
import FundingStatePanel from './FundingStatePanel';
import RealizationSatePanel from './RealizationStatePanel';
import UnsuccessfullStatePanel from './UnsuccessfullStatePanel';


export default function StatePanel({project}) {
	switch(project.projectState) {
		case 'STUDY':
			return <StudyStatePanel />;
		case 'FEASIBLE':
			return <FeasibleStatePanel />;
		case 'IN_FUNDING':
			return <FundingStatePanel progress={project.progress} />;
		case 'IN_REALIZATION':
			return <RealizationSatePanel />;
		case 'UNSUCCESSFULL':
			return <UnsuccessfullStatePanel />;
		default:
			return null;
	}
}