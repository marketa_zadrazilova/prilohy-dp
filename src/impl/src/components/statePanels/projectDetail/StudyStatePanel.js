import React from 'react';
import { Button } from 'react-bootstrap';
import StatePanelLayout from './StatePanelLayout';
import Voting from '../../Voting';
import FollowButton from '../../common/FollowButton';


export default function StudyStatePanel(props) {
	return (
		<StatePanelLayout
			title="Projekt je ve fázi studie proveditelnosti"
			description="V této fázi zástupci města posuzují projekt z hlediska jeho realizovatelnosti.
			Pokud bude projekt posouzen jako realizovatelný, může být zahájena sbírka prostředků 
			pro jeho realizaci."
		>
			<br />
			<h5>Chcete být informován o novinkách?</h5>
			<FollowButton />

			<br />
			<Voting />
		</StatePanelLayout>
	);
}