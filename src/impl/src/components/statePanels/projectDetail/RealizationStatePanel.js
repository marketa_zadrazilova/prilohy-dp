import React from 'react';
import StatePanelLayout from './StatePanelLayout';
import FollowButton from '../../common/FollowButton';


export default function RealizationStatePanel(props) {
    return (
        <StatePanelLayout
            title="Projekt byl úspěšně ukončen"
            description="Podařilo se získat všechny prostředky potřebné pro realizaci projektu."
        >
            <FollowButton />
        </StatePanelLayout>
    )
}