import React from 'react';
import { Button } from 'react-bootstrap';
import Voting from '../../Voting';
import StatePanelLayout from './StatePanelLayout';
import FollowButton from '../../common/FollowButton';


export default function FeasibleStatePanel(props) {
	return (
		<StatePanelLayout
			title="Projekt je realizovatelný"
			description="Zástupci města shledali tento projekt jako realizovatelný. V tuto chvíli projekt čeká, až autor zahájí sbírku."
		>
			<br />
			<h5>Chcete být notifikován o změnách?</h5>
			<FollowButton />

			<br />
			<Voting />
		</StatePanelLayout>
	);
}