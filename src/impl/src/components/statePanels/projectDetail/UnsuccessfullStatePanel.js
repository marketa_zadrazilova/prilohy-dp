import React from 'react';
import StatePanelLayout from './StatePanelLayout';


export default function UnsuccessfulStatePanel(props) {
    return (
        <StatePanelLayout
            title="Projekt je neúspěšný"
            description="Na realizaci projektu se nepodařilo vybrat prostředky."
        />
    )
}