import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import Progress from '../../Progress';
import Voting from '../../Voting';
import StatePanelLayout from './StatePanelLayout';


export default function FundingStatePanel(props) {
	return (
		<StatePanelLayout
			title="Projekt je ve fázi sbírka"
			description="V této fázi projekt sbírá prostředky pro svou realizaci."
		>
			<Progress progress={props.progress} />

			Projekt můžete podpořit těmito způsoby:
			<ul>
				<li>Finančně</li>
				<li>Materiálem</li>
				<li>Jako dobrovolník</li>
			</ul>

			<Voting />
		</StatePanelLayout>
	);
}

FundingStatePanel.propTypes = {
	progress: PropTypes.object.isRequired,
}