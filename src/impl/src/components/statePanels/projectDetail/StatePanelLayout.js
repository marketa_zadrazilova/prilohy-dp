import React from 'react';
import PropTypes from 'prop-types';
import { Panel } from 'react-bootstrap';


export default function StatePanelLayout({
    title,
    description,
    children
}) {
    return (
        <Panel className="state-panel">
            <h4>{title}</h4>
            <p>{description}</p>

            { children }
        </Panel>
    );
}

StatePanelLayout.props = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
}