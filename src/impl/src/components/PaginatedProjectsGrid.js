import React from 'react';
import PropTypes from 'prop-types';
import ProjectsGrid from './ProjectsGrid';
import Pagination from './Pagination';


export default class PaginatedProjectsGrid extends React.Component {
	constructor(props) {
		super(props);

		const {
			page,
			pageSize
		} = props;

		this.state = {
			page,
			pageSize
		};

		this.handlePrev = this.handlePrev.bind(this);
		this.handleNext = this.handleNext.bind(this);
	}

	handlePrev() {
		this.setState({
			page: this.state.page - 1,
		});
	}

	handleNext() {
		this.setState({
			page: this.state.page + 1,
		});
	}

	render() {
		const {
			page,
			pageSize
		} = this.state;

		const total = this.props.projects.length;
		const firstIndex = total === 0 ? 0 : (page - 1) * pageSize + 1;
		const lastIndex = (firstIndex + pageSize - 1 > total) ? total : firstIndex + pageSize - 1;

		const visibleProjects = this.props.projects.slice(firstIndex - 1, lastIndex);

		return (
			<div>
				<Pagination 
					firstIndex={firstIndex}
					lastIndex={lastIndex}
					total={total}
					onPrevious={this.handlePrev}
					onNext={this.handleNext}
				/>
				
				<ProjectsGrid 
					projects={visibleProjects} 
				/>
				
				<Pagination 
					firstIndex={firstIndex}
					lastIndex={lastIndex}
					total={total}
					onPrevious={this.handlePrev}
					onNext={this.handleNext}
				/>
			</div>
		);
	}
}

PaginatedProjectsGrid.defaultProps = {
	page: 1,
	pageSize: 6
};

PaginatedProjectsGrid.propTypes = {
	projects: PropTypes.array.isRequired,
	page: PropTypes.number,
	pageSize: PropTypes.number
};