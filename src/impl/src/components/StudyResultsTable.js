import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';


export default function StudyResultsTable(props) {
    const rows = props.results.map(({subject, resolution, result}, idx) => (
        <tr key={idx}>
            <td>{subject}</td>
            <td>{resolution}</td>
            <td>{result}</td>
        </tr>
    ));

    return (
        <Table>
            <thead>
                <tr>
                    <th>Subjekt</th>
                    <th>Vyjádření</th>
                    <th>Výsledek</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </Table>
    );
}

StudyResultsTable.propTypes = {
    results: PropTypes.arrayOf(PropTypes.shape({
        subject: PropTypes.string.isRequired,
        resolution: PropTypes.string.isRequired,
        result: PropTypes.string.isRequired
    }))
};

StudyResultsTable.defaultProps = {
    results: [{
        subject: "Majetkový odbor",
        resolution: "Jedná se o majetek města svěřený městské části. Povolení může být uděleno.",
        result: "Schváleno"
    }, {
        subject: "Odbor životního prostředí",
        resolution: "Projekt je v souladu se strategií odboru životního prostředí. ",
        result: "Schváleno"
    }]
};