import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { Glyphicon, Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import SearchBox from '../SearchBox';


export default function Header(props) {
	const { name, isAuthenticated } = props.auth;

	const username = <span><Glyphicon glyph="user" /> {name}</span>

	const userLinks = (
		<Nav pullRight>
			<NavDropdown eventKey={3} title={username} id="basic-nav-dropdown">
				<LinkContainer to="/autor/muj-profil">
					<MenuItem eventKey={3.1}>Můj profil</MenuItem>
				</LinkContainer>
				<NavItem eventKey={3.2} onClick={props.onLogout}>Odhlásit se</NavItem>
			</NavDropdown>
		</Nav>
	);

	const guestLinks = (
		<Nav pullRight>
			<LinkContainer to="/prihlaseni">
				<NavItem eventKey={1}>Přihlášení</NavItem>
			</LinkContainer>
			<LinkContainer to="/registrace">
				<NavItem eventKey={2}>Registrace</NavItem>
			</LinkContainer>
		</Nav>
	);

	return (
		<Navbar>
			<Navbar.Header collapseOnSelect>
				<Navbar.Brand>
					<Link to="/">CityFunder</Link>
				</Navbar.Brand>
				 <Navbar.Toggle />
			</Navbar.Header>
			<Navbar.Collapse>
				<Nav>
					<LinkContainer to="/projekty">
						<NavItem eventKey={4}>Projekty</NavItem>
					</LinkContainer>
					<LinkContainer to="/novy-projekt">
						<NavItem eventKey={5}>Vytvořit projekt</NavItem>
					</LinkContainer>
					{ isAuthenticated &&
						<LinkContainer to="/autor/projekty">
							<NavItem eventKey={6}>Moje projekty</NavItem>
						</LinkContainer>
					}
					<LinkContainer to="/jak-to-funguje">
						<NavItem eventKey={7}>Jak to funguje?</NavItem>
					</LinkContainer>
				</Nav>
				{ isAuthenticated ? userLinks : guestLinks }
				<Navbar.Form pullRight>
					<SearchBox />	
				</Navbar.Form>
			</Navbar.Collapse>
		</Navbar>
	);
}

Header.propTypes = {
	auth: PropTypes.object.isRequired,
	onLogout: PropTypes.func.isRequired
};