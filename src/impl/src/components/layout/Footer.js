import React from 'react';
import { Col, Grid, Row } from 'react-bootstrap';


export default function Footer(props) {
	return (
		<footer className="sticky-footer">
			<Grid>
				<Row>
					<Col>
						<p>CityFunder &copy; 2017</p>
					</Col>
				</Row>
			</Grid>
		</footer>
	);
}