import React from 'react';
import PropTypes from 'prop-types';
import { format } from '../../utils/DateUtils';

export default function DateFormatter(props) {
    const { date } = props;
    
    if (!date) {
        return null;
    }

    return (
        <span>{format(date)}</span>
    );
}

DateFormatter.propTypes = {
    date: PropTypes.string
};