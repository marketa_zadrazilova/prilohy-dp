import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Glyphicon } from 'react-bootstrap';


export default function InfoBox(props) {
    return (
        <Alert bsStyle="info">
            <p>
                <strong><Glyphicon glyph='info-sign'/> {props.title}</strong>
            </p>
            { props.children }
            { props.text && 
                <p>{props.text}</p>
            }
        </Alert>
    );
}

InfoBox.props = {
    title: PropTypes.string.isRequired,
    text: PropTypes.string
};