import React from 'react';
import { Button } from 'react-bootstrap';


export default class FollowButton extends React.Component {
    state = {
        isFollowing: false
    };

    handleClick = () => {
        this.setState({isFollowing: !this.state.isFollowing});
    }

    render() {
        return (
            <Button 
                bsStyle="warning"
                onClick={this.handleClick}
            >
                { this.state.isFollowing ? "Přestat sledovat projekt" : "Sledovat projekt" }
            </Button>
        )
    }
}