import React from 'react';
import PropTypes from 'prop-types';
import { Label } from 'react-bootstrap';
import { getCategoryLabel } from '../../constants/Categories';


export default function CategoriesList({categories}) {
    const labels = categories.map((category, idx) => <Label key={idx}>{getCategoryLabel(category)}</Label>);

    return (
        <div className="categories-list">
            <p className="categories-list-title">Kategorie:</p>
            {labels}
        </div>
    );
}

CategoriesList.props = {
    categories: PropTypes.arrayOf(PropTypes.string).isRequired
};