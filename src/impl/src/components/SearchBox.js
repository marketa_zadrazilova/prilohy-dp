import React from 'react';
import { Redirect } from 'react-router-dom';
import { Button, FormControl, FormGroup, Glyphicon, InputGroup } from 'react-bootstrap';


export default class SearchBox extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			query: '',
			redirect: false,
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
	}

	handleChange(e) {
		e.preventDefault();

		this.setState({
			query: e.target.value
		});
	}

	handleSearch(e) {
		e.preventDefault();

		if (this.state.query !== '') {
			this.setState({redirect: true});
		} else {
			alert('Search query is empty');
			this.setState({redirect: false});
		}
	}

	render() {
		if (this.state.redirect && this.state.query) {
			this.setState({redirect: false});

			return <Redirect to={{
				pathname: "/vyhledavani",
				state: {query: this.state.query}
			}} />
		}

		return (
			<FormGroup id="search-box">
				<InputGroup>
					<FormControl value={this.state.query} onChange={this.handleChange} type="text" />
					<InputGroup.Button>
						<Button onClick={this.handleSearch}>
							<Glyphicon glyph="search" />
						</Button>
					</InputGroup.Button>
				</InputGroup>
			</FormGroup>
		);
	}
}