import React from 'react';


const CustomTab = ({
	title,
	icon,
	key,
	color,
	onClick,
}) => {
	return <li role="presentational" key={key}>{title} {icon}</li>
};

export default CustomTab;