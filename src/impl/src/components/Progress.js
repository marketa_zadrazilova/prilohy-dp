import React from 'react';
import PropTypes from 'prop-types';
import { ProgressBar, OverlayTrigger, Popover, Label } from 'react-bootstrap';

function getPercentage(pledged, goal) {
	return Math.floor(pledged / (goal / 100));
}

export default function Progress(props) {
	const { pledged, pledgedByCity, goal } = props.progress;
	const daysLeft = 12;

	const sum = pledged + (pledgedByCity ? pledgedByCity : 0);

	const popoverHoverFocus = (
	  <Popover id="popover-trigger-hover-focus">
	    <ul>
		    <li><Label bsStyle="info">Město:</Label> {pledgedByCity} Kč</li>
		    <li><Label bsStyle="success">Uživatelé:</Label> {pledged} Kč</li>
	    </ul>
	  </Popover>
	);

	return (
		<div className="my-progress-bar clearfix">
			<strong>{getPercentage(sum, goal)} %</strong>
			<span className="pull-right">Vybráno {sum} Kč z {goal} Kč</span>
			<OverlayTrigger trigger={['hover', 'focus']} placement="bottom" overlay={popoverHoverFocus}>
				<ProgressBar>
					<ProgressBar 
						bsStyle="info" 
						now={getPercentage(pledgedByCity? pledgedByCity:0, goal)} 
					/>
					<ProgressBar
						bsStyle="success" 
						now={getPercentage(pledged, goal)}
					/>
				</ProgressBar>
			</OverlayTrigger>
			<p className="pull-right">{daysLeft} dní do konce sbírky</p>
		</div>
	);
}

Progress.propTypes = {
	progress: PropTypes.object.isRequired
};

/**
Potrebuje parametry:
endDate - datum ukončení sbírky (datum a čas) -> pro spočítání rozdílu
goal - celková cílová částka
pledged - kolik je vybráno

případně
pledged {
	municipality: 10 000,
	financially: 2 000,
	in material: 5 450,
	volunteers: 700
}
*/