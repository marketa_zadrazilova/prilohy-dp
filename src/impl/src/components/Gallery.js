import React from 'react';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';
import { Image } from 'react-bootstrap';


export default class Gallery extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false,
			currentIdx: 0,
		};

		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.handleClickNext = this.handleClickNext.bind(this);
		this.handleClickPrev = this.handleClickPrev.bind(this);
	}

	open(idx) {
		this.setState({
			currentIdx: idx,
			isOpen: true,
		});
	}

	close () {
		this.setState({
			isOpen: false,
			currentIdx: 0
		});
	}

	handleClickNext() {
		var idx = this.state.currentIdx;

		this.setState({
			currentIdx: idx + 1
		})
	}

	handleClickPrev() {
		var idx = this.state.currentIdx;

		this.setState({
			currentIdx: idx - 1
		})
	}

	render() {
		const gallery = this.props.gallery.map((image, idx) => {
			return (
				<div className="galleryThumb" key={idx} onClick={() => {this.open(idx)}}>
					<Image className="object-fit_cover" width="360px" height="260px" src={image.src} />
				</div>
			);
		});

		return (
			<div>
				<h3>Galerie</h3>
				{ gallery }
				<Lightbox 
					images={this.props.gallery}
					isOpen={this.state.isOpen}
					currentImage={this.state.currentIdx}
					onClickNext={this.handleClickNext}
					onClickPrev={this.handleClickPrev}
					onClose={this.close}
				/>
			</div>
		);
	}
}

Gallery.propTypes = {
	gallery: PropTypes.array.isRequired,
};