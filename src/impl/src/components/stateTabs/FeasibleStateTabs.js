import React from 'react';
import PropTypes from 'prop-types';
import { Col, Nav, NavItem, Row, Tab, Button } from 'react-bootstrap';
import ProjectDetailsForm from '../ProjectDetailsForm';
import ProjectLocationForm from '../ProjectLocationForm';
import CostsTable from '../costs/CostsTable';
import ManageNewsContainer from '../../containers/ManageNewsContainer';
import StudyResultsTable from '../StudyResultsTable';
import InfoBox from '../common/InfoBox';
import ProjectInfoTab from './ProjectInfoTab';

import { FormControl, FormGroup, ControlLabel, Checkbox } from 'react-bootstrap';


export default function FeasibleStateTabs({project, costs, news}) {
    return (
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
			<Row className="clearfix">
				<Col sm={3}>
					<Nav bsStyle="pills" stacked>
						<NavItem eventKey="first">
							Informace o projektu
						</NavItem>
						<NavItem eventKey="news">
							Správa novinek
						</NavItem>
						<NavItem eventKey="study">
							Vyjádření města
						</NavItem>
						<NavItem eventKey="funding">
							Sbírka
						</NavItem>
					</Nav>
				</Col>
				<Col sm={9}>
					<Tab.Content animation>
						<Tab.Pane eventKey="first">
							<ProjectInfoTab 
								project={project}
								costs={costs}
							/>
						</Tab.Pane>
						<Tab.Pane eventKey="study">
							<h3>Vyjádření města</h3>
							<StudyResultsTable />
						</Tab.Pane>	
						<Tab.Pane eventKey="funding">
							<h3>Sbírka</h3>

							<form>
								<FormGroup>
									<ControlLabel>Doba trvání sbírky</ControlLabel>
									<FormControl componentClass="select" placeholder="Vyberte dobu trvání sbírky" >
										<option value="">2 týdny</option>
										<option value="">3 týdny</option>
										<option value="">měsíc</option>
									</FormControl>
								</FormGroup>
								<FormGroup>
									<ControlLabel>Způsob přispívání</ControlLabel>
									<Checkbox>
										Finančně
									</Checkbox>
									{' '}
									<Checkbox>
										Materiálně
									</Checkbox>
									{' '}
									<Checkbox>
										Dobrovolníci
									</Checkbox>
								</FormGroup>
							</form>

							<Button>Spustit sbírku</Button>
						</Tab.Pane>
						<Tab.Pane eventKey="news">
							<h3>Správa novinek</h3>

							<ManageNewsContainer />
						</Tab.Pane>
					</Tab.Content>
				</Col>
			</Row>
		</Tab.Container>
	);
}

FeasibleStateTabs.props = {
	project: PropTypes.object.isRequired,
	costs: PropTypes.array.isRequired,
	news: PropTypes.array.isRequired
};