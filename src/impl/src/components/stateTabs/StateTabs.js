import React from 'react';
import NewProjectContainer from '../../containers/NewProjectContainer';
import AwaitingStateTabs from './AwaitingStateTabs';
import StudyStateTabs from './StudyStateTabs';
import FeasibleStateTabs from './FeasibleStateTabs';
import FundingStateTabs from './FundingStateTabs';


export default function StateTabs({project, costs, news}) {
	const state = project.projectState;

	switch(state) {
		case 'NEW':
			return <NewProjectContainer project={project} />;
		case 'AWAITING':
			return <AwaitingStateTabs project={project} costs={costs} news={news} />
		case 'STUDY':
			return <StudyStateTabs project={project} costs={costs} news={news} />
		case 'FEASIBLE':
			return <FeasibleStateTabs project={project} costs={costs} news={news} />
		case 'IN_FUNDING':
			return <FundingStateTabs project={project} costs={costs} news={news} />
		default:
			return null;
	}
}