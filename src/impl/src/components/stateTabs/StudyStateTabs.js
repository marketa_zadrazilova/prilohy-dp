import React from 'react';
import PropTypes from 'prop-types';
import { Col, Nav, NavItem, Row, Tab } from 'react-bootstrap';
import ProjectDetailsForm from '../ProjectDetailsForm';
import ProjectLocationForm from '../ProjectLocationForm';
import CostsTable from '../costs/CostsTable';
import ProjectInfoTab from './ProjectInfoTab';
import ManageNewsContainer from '../../containers/ManageNewsContainer';
import InfoBox from '../common/InfoBox';

export default function StudyStateTabs({project, costs, news}) {
    return (
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
			<Row className="clearfix">
				<Col sm={3}>
					<Nav bsStyle="pills" stacked>
						<NavItem eventKey="first">
							Informace o projektu
						</NavItem>
						<NavItem eventKey="news">
							Správa novinek
						</NavItem>
					</Nav>
				</Col>
				<Col sm={9}>
					<Tab.Content animation>
						<Tab.Pane eventKey="first">
							<ProjectInfoTab 
								project={project}
								costs={costs}
							/>
						</Tab.Pane>
						<Tab.Pane eventKey="news">
							<h3>Správa novinek</h3>

							<ManageNewsContainer />
						</Tab.Pane>
					</Tab.Content>
				</Col>
			</Row>
		</Tab.Container>
	);
}

StudyStateTabs.props = {
	project: PropTypes.object.isRequired,
	costs: PropTypes.array.isRequired,
	news: PropTypes.array.isRequired
};