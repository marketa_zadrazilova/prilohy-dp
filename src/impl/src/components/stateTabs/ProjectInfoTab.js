import React from 'react';
import ProjectDetailsForm from '../ProjectDetailsForm';
import ProjectLocationMap from '../maps/ProjectLocationMap';
import CostsTable from '../costs/CostsTable';
import InfoBox from '../common/InfoBox';


export default function ProjectInfoTab({ project, costs }) {
    return (
        <div>
            <h3>Informace o projektu</h3>
            <p>Zde se nachází informace o projektu, které byly zadány při vytvoření projektu</p>
            
            <br/>
            <h4>Detaily projektu</h4>

            <table>
                <tr>
                    <th>Název projektu</th>
                    <td>{project.name}</td>
                </tr>
                <tr>
                    <th>Kategorie</th>
                    <td>{project.categories}</td>
                </tr>
                <tr>
                    <th>Krátký popis</th>
                    <td>{project.descriptionShort}</td>
                </tr>
                <tr>
                    <th>Dlouhý popis</th>
                    <td>{project.descriptionLong}</td>
                </tr>
            </table>
            <hr/>
            <br/>
            <h4>Lokace</h4>
            <ProjectLocationMap location={project.location} />
            <hr/>
            <br/>
            <h4>Náklady</h4>
            <CostsTable costs={costs} />
        </div>
    );
}