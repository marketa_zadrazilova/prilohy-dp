import React from 'react';
import PropTypes from 'prop-types';
import { Col, Nav, NavItem, Row, Tab, Button } from 'react-bootstrap';
import ProjectDetailsForm from '../ProjectDetailsForm';
import ProjectLocationForm from '../ProjectLocationForm';
import CostsTable from '../costs/CostsTable';
import ManageNewsContainer from '../../containers/ManageNewsContainer';
import StudyResultsTable from '../StudyResultsTable';
import InfoBox from '../common/InfoBox';
import ProjectInfoTab from './ProjectInfoTab';

import { FormControl, FormGroup, ControlLabel, Checkbox } from 'react-bootstrap';

export default function FundingStateTabs({project, costs}) {
    return (
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row className="clearfix">
                <Col sm={3}>
                    <Nav bsStyle="pills" stacked>
                        <NavItem eventKey="first">
                            Informace o projektu
                        </NavItem>
                        <NavItem eventKey="news">
                            Správa novinek
                        </NavItem>
                        <NavItem eventKey="study">
                            Vyjádření města
                        </NavItem>
                        <NavItem eventKey="funding">
                            Sbírka
                        </NavItem>
                    </Nav>
                </Col>
                <Col sm={9}>
                    <Tab.Content animation>
                        <Tab.Pane eventKey="first">
                            <ProjectInfoTab 
                                project={project} 
                                costs={costs} 
                            />
                        </Tab.Pane>
                        <Tab.Pane eventKey="study">
                            <h3>Vyjádření města</h3>
                            <StudyResultsTable />
                        </Tab.Pane>	
                        <Tab.Pane eventKey="funding">
                            <h3>Sbírka</h3>
                        </Tab.Pane>
                        <Tab.Pane eventKey="news">
                            <h3>Správa novinek</h3>

                            <ManageNewsContainer />
                        </Tab.Pane>
                    </Tab.Content>
                </Col>
            </Row>
        </Tab.Container>
    );
}