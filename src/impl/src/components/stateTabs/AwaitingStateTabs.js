import React from 'react';
import PropTypes  from 'prop-types';
import { Col, Nav, NavItem, Row, Tab } from 'react-bootstrap';
import ProjectDetailsForm from '../ProjectDetailsForm';
import ProjectLocationForm from '../ProjectLocationForm';
import CostsTable from '../costs/CostsTable';
import InfoBox from '../common/InfoBox';


export default function AwaitingStateTabs({project, costs, news}) {
	return (
		<Tab.Container id="left-tabs-example" defaultActiveKey="first">
			<Row className="clearfix">
				<Col sm={3}>
					<Nav bsStyle="pills" stacked>
						<NavItem eventKey="first">
							Detaily
						</NavItem>
						<NavItem eventKey="second">
							Umístění
						</NavItem>
						<NavItem eventKey="third">
							Náklady
						</NavItem>
					</Nav>
				</Col>
				<Col sm={9}>
					<Tab.Content animation>
						<Tab.Pane eventKey="first">
							<h3>Detaily projektu</h3>

							<InfoBox title="Zadané údaje již není možné upravovat." />

							<ProjectDetailsForm
								isEditable={false}
								projectDetails={project}
								onItemChange={()=>{}}
							/>
						</Tab.Pane>
						<Tab.Pane eventKey="second">
							<h3>Lokace</h3>

							<InfoBox title="Zadané údaje již není možné upravovat." />

							<ProjectLocationForm 
								isEditable={false}
								location={project.location} 
								onLocationChange={()=>{}}
							/>
						</Tab.Pane>
						<Tab.Pane eventKey="third">
							<h3>Náklady</h3>

							<InfoBox title="Zadané údaje již není možné upravovat." />

							<CostsTable costs={costs} />
						</Tab.Pane>
					</Tab.Content>
				</Col>
			</Row>
		</Tab.Container>
	);
}

AwaitingStateTabs.propTypes = {
	project: PropTypes.object.isRequired,
};