import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from 'react-bootstrap';
import FieldGroup from './FieldGroup';


export default function LabeledInput({
	id,
	isRequired,
	label,
	validation,
	...props
}) {
	return (
		<FieldGroup 
			id={id}
			isRequired={isRequired}
			label={label}
			validation={validation}
		>
			<FormControl
				type={props.type}
				name={props.name}
				onChange={props.onChange}
				value={props.value}
				{...props}
			/>
		</FieldGroup>
	);
}

LabeledInput.propTypes = {
	id: PropTypes.string.isRequired,
	isRequired: PropTypes.bool,
	label: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	value: PropTypes.string,
};