import React from 'react';
import PropTypes from 'prop-types';
import { ControlLabel, FormGroup, HelpBlock } from 'react-bootstrap';


function getValidationState(validation) {
	if (!validation) return null;

	if (validation) {
		return 'error';
	}
}

export default function FieldGroup(props) {
	return (
		<FormGroup 
			controlId={props.id}
			validationState={getValidationState(props.validation)}
		>
			<ControlLabel>{props.label}</ControlLabel>
			{props.isRequired && 
				<span className="pull-right">(povinné)</span>
			}
			{props.children}
			{props.validation &&
				<HelpBlock>{props.validation}</HelpBlock>
			}
		</FormGroup>
	);
}

FieldGroup.propTypes = {
	id: PropTypes.string,
	isRequired: PropTypes.bool,
	label: PropTypes.string,
	validation: PropTypes.string,
};