import React from 'react';
import PropTypes from 'prop-types';
import FieldGroup from './FieldGroup';
import Password from './Password';


export default function LabeledPassword(props) {
	return (
		<FieldGroup 
			{...props}
		>
			<Password 
				name={props.name}
				onChange={props.onChange}
				value={props.value} 
			/>
		</FieldGroup>
	);
}

LabeledPassword.propTypes = {
	id: PropTypes.string,
	isRequired: PropTypes.bool,
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func,
	value: PropTypes.string,
};