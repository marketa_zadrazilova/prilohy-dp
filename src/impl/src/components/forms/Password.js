import React from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon, InputGroup, FormControl } from 'react-bootstrap';


export default class Password extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			show: false
		}

		this.toggleVisibility = this.toggleVisibility.bind(this);
	}

	toggleVisibility() {
		this.setState({
			show: !this.state.show
		});
	}

	render() {
		var type = this.state.show ? 'text' : 'password'; 
		var icon = <Glyphicon glyph={this.state.show ? 'eye-close' : 'eye-open'} />;

		return (
			<InputGroup>
				<FormControl 
					type={type} 
					name={this.props.name}
					onChange={this.props.onChange} 
					value={this.props.value} 
				/>
				<InputGroup.Button>
					<Button onClick={this.toggleVisibility}>{icon}</Button>
				</InputGroup.Button>
			</InputGroup>
		);
	}
}

Password.propTypes = {
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string,
};