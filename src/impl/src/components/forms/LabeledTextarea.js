import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from 'react-bootstrap';
import FieldGroup from './FieldGroup';


export default function LabeledTextarea(props) {
	return (
		<FieldGroup 
			{...props}
		>
			<FormControl
				componentClass="textarea"
				name={props.name}
				onChange={props.onChange}
				value={props.value}
				{...props}
			/>
		</FieldGroup>
	);
}

LabeledTextarea.propTypes = {
	id: PropTypes.string.isRequired,
	isRequired: PropTypes.bool,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
};