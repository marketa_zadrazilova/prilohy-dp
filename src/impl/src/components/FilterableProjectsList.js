import React from 'react';
import PropTypes from 'prop-types';
import FilterBar from './FilterBar';
import PaginatedProjectsGrid from './PaginatedProjectsGrid';


export default class FilterableProjectsList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			filterBy: {
				state: 'ALL',
				type: 'ALL',
			},
			sortBy: 'nameAsc',
		};

		this.setFilter = this.setFilter.bind(this);
		this.setSortBy = this.setSortBy.bind(this);
	}

	filterBy(array, key, value) {
		if (value === 'ALL') {
			return array;
		}

		if (key === 'type') {
			return array.filter(project => project.categories.includes(value));
		}

		return array.filter((item) => item[key] === value);
	}

	filter(projects) {	
		var filtered;	
		
		filtered = this.filterBy(projects, 'projectState', this.state.filterBy.state);
		console.log(filtered);
		filtered = this.filterBy(filtered, 'type', this.state.filterBy.type);

		console.log(this.state.filterBy.type);

		return filtered;
	}

	setFilter(key, value) {
		this.setState({
			filterBy: {
				...this.state.filterBy,
				[key]: value
			}
		});
	}

	sortByName(first, second) {
		const firstLC = first.toLowerCase();
		const secondLC = second.toLowerCase();

		if (firstLC < secondLC) {
			return -1;
		}

		if (firstLC > secondLC) {
			return 1;
		}

		return 0;
	}

	sort(array, sortBy) {
		if (sortBy === 'nameAsc') {
			return array.sort((a, b) => this.sortByName(a.name, b.name));
		}

		if (sortBy === 'nameDesc') {
			return array.sort((a,b) => this.sortByName(b.name, a.name));
		}

		return array;
 	}

	setSortBy(e) {
		this.setState({sortBy: e.target.value});
	}

	render () {
		const {
			projects
		} = this.props;

		const filteredProjects = this.filter(projects);
		const sortedProjects = this.sort(filteredProjects, this.state.sortBy);

		return (
			<div>				
				<FilterBar 
					onFilter={this.setFilter}
					onSort={this.setSortBy}
				/>
				
				<PaginatedProjectsGrid projects={sortedProjects} pageSize={9} />
			</div>
		);
	}
}

FilterableProjectsList.propTypes = {
	projects: PropTypes.array.isRequired
};