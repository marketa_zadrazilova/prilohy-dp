import React from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';


export default function FormPaneLayout(props) {
	const {
		description,
		isFirst,
		isLast,
		title,
		onPrev,
		onSave,
		onNext,
	} = props;

	return (
		<div className="formPane">
			<h3>{title}</h3>

			<p>
				{description}
			</p>

			{ props.children }

			<div className="pagerBar">
				{!isFirst &&
					<Button onClick={onPrev}><Glyphicon glyph="arrow-left"/> Předchozí</Button>
				}

				<div className="pull-right">
					<Button onClick={onSave}><Glyphicon glyph="floppy-disk"/> Uložit</Button>
					{!isLast &&
						<Button onClick={onNext}>Uložit a pokračovat <Glyphicon glyph="arrow-right"/></Button>
					}
				</div>
			</div>
		</div>
	);
}

FormPaneLayout.propTypes = {
	description: PropTypes.string.isRequired,
	isFirst: PropTypes.bool,
	isLast: PropTypes.bool,
	onSave: PropTypes.func.isRequired,
	onPrev: PropTypes.func.isRequired,
	onNext: PropTypes.func.isRequired,
	title: PropTypes.string.isRequired,
};