import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'react-bootstrap';
import ProjectTile from './ProjectTile';


export default class ProjectsGrid extends React.Component {
	render () {
		const projects = this.props.projects.map((project) => <ProjectTile key={project.id} project={project}/>);

		if (projects.length === 0) {
			return <Row><p className="text-muted text-center">Nebyly nalezeny žádné projekty.</p></Row>
		}

		return <Row>{projects}</Row>;
	}
}

ProjectsGrid.propTypes = {
	projects: PropTypes.array.isRequired,
};