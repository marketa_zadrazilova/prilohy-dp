import React from 'react';


export default class UpdatableImage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			file: {},
			imageSrc: '/img/profile-w.svg',
		};

		this.onClick = this.onClick.bind(this);
		this.handleImageChange = this.handleImageChange.bind(this);
	}

	onClick() {
		this.fileInput.click();
	}

	handleImageChange(e) {
		e.preventDefault();

		const reader = new FileReader();
		const file = e.target.files[0];

		reader.onloadend = () => {
			this.setState({
				file: file,
				imageSrc: reader.result,
			});
		};

		reader.readAsDataURL(file);
	}

	render() {
		return (
			<div className="updatable-img" onClick={this.onClick}>
				<img width={130} height={130} className="object-fit_cover" src={this.state.imageSrc} alt=""/>
				<div className="overlay">
					<div className="text">Změnit fotku</div>
				</div>
				<input 
					className="hidden" 
					type="file"
					ref={(input) => {this.fileInput = input;}}
					onChange={this.handleImageChange} 
				/>
			</div>
		);
	}
}