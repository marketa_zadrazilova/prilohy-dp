import React from 'react';
import { Modal } from 'react-bootstrap';
import SignUpContainer from '../../containers/SignUpContainer';

export default function SignUpModal(props) {
    return (
        <Modal show={props.show} onHide={props.onHide}>
            <Modal.Header closeButton />
            <Modal.Body>
                <SignUpContainer />
            </Modal.Body>
        </Modal>
    );
}