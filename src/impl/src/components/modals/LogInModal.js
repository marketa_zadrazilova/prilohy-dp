import React from 'react';
import { Modal } from 'react-bootstrap';
import LogInContainer from '../../containers/LogInContainer';


export default function LogInModal(props) {
    return (
        <Modal show={props.show} onHide={props.onHide}>
            <Modal.Header closeButton />
            <Modal.Body>
                <LogInContainer />
            </Modal.Body>
        </Modal>
    );
}