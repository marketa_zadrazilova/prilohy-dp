import React from 'react';
import PropTypes from 'prop-types';


export default function PageLoader({show}) {
	if (show) {
		return (
			<div className="loader-wrapper">
				<div className="loader"></div>
			</div>
		);
	}

	return null;
}

PageLoader.propTypes = {
	show: PropTypes.bool.isRequired,
}

PageLoader.defaultProps = {
	show: false,
}