import React from 'react';
import { Button, Glyphicon, Modal } from 'react-bootstrap';


export default class CancelProjectConfirmation extends React.Component {
    state = {
        showModal: false,
    };

    hide = () => {
        this.setState({showModal: false});
    }

    show = () => {
        this.setState({showModal: true});
    }

    render() {
        return (
            <div>
                <Button
                    bsStyle = "danger"
                    onClick = {this.show}
                    block
                >
                    <Glyphicon glyph="trash"/> Zrušit projekt
                </Button>
                <Modal show={this.state.showModal} onHide={this.hide}>
                    <Modal.Header closeButton>
                        <Modal.Title>Zrušení projektu</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Opravdu si přejete zrušit projekt? Zrušení projektu je nevratná akce.
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="link" onClick={this.hide}>Zpět</Button>
                        <Button bsStyle="danger" onClick={this.hide}>Zrušit projekt</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}