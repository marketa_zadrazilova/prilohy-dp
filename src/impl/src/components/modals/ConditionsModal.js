import React from 'react';
import { Modal } from 'react-bootstrap'; 

export default class ConditionsModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false
        };

        this.toggleVisibility = this.toggleVisibility.bind(this);
    }

    toggleVisibility() {
        const prev = this.state.isVisible;

        this.setState({
            isVisible: !prev
        });
    }

    render() {
        return (
            <div>
                <p>Registrací souhlasím s <a onClick={this.toggleVisibility}>podmínkami CityFunderu.</a></p>

                <Modal show={this.state.isVisible} onHide={this.toggleVisibility}>
                    <Modal.Header closeButton>
                        <Modal.Title>Podmínky portálu CityFunder</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Zde budou uvedeny podmínky a pravidla používání portálu CityFunder.
                        Lorem ipsum dolor sit amet ...
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}