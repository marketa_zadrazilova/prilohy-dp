import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { getSumOfCosts } from '../../utils/Common.js';


export default function CostsTable({
	costs
}) {
	const rows = costs.map((cost, idx) => {
		return (
			<tr key={idx}>
				<td>{cost.title}</td>
				<td>{cost.unitCount * cost.unitCost}</td>
			</tr>
		);
	});

	const sum = getSumOfCosts(costs);

	return (
		<Table>
			<thead>
				<tr>
					<th>Položka</th>
					<th>Cena v Kč</th>
				</tr>
			</thead>
			<tbody>
				{rows}
			</tbody>
			<tfoot>
				<tr>
					<th>Celkem:</th>
					<th>{sum} Kč</th>
				</tr>
			</tfoot>
		</Table>
	);
}

CostsTable.propTypes = {
	costs: PropTypes.array.isRequired,
};