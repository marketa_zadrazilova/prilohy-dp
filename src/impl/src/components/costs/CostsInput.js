import React from 'react';
import PropTypes from 'prop-types';
import { Button, Table, FormControl } from 'react-bootstrap';
import { getSumOfCosts } from '../../utils/Common.js';


export default function CostsInput({
	costs,
	input,
	onInputChange,
	onAddCost,
	onRemoveCost,
}) {
	const rows = costs.map((cost, idx) => {
		return (
			<tr key={idx}>
				<td>{cost.title}</td>
				<td>{cost.unitCost}</td>
				<td>{cost.unitCount}</td>
				<td>{cost.unit}</td>
				<td>{cost.unitCost * cost.unitCount}</td>
				<td><Button onClick={(e) => onRemoveCost(cost)}>Odebrat</Button></td>
			</tr>
		);
	});

	const sum = getSumOfCosts(costs);
	const disableAddButton = input.itemTitle === '' || input.itemCost === '';

	return (
		<Table>
			<thead>
				<tr>
					<th>Název</th>
					<th>Cena za jednotku</th>
					<th>Počet jednotek</th>
					<th>Název jednotky</th>
					<th>Cena</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{rows}
				<tr>
					<td>
						<FormControl
							type="text"
							name="itemTitle"
							value={input.itemTitle}
							onChange={onInputChange}
						/>
					</td>
					<td>
						<FormControl
							type="number"
							name="itemUnitCost"
							value={input.itemUnitCost}
							onChange={onInputChange}
						/>
					</td>
					<td>
						<FormControl
							type="number"
							name="itemUnitCount"
							value={input.itemUnitCount}
							onChange={onInputChange}
						/>
					</td>
					<td>
						<FormControl
							type="text"
							name="itemUnit"
							value={input.itemUnit}
							onChange={onInputChange}
						/>
					</td>
					<td>
						<FormControl.Static>
							{input.itemUnitCost * input.itemUnitCount}
						</FormControl.Static>
					</td>
					<td>
						<Button 
							onClick={onAddCost}
							disabled={disableAddButton}
						>
							Přidat
						</Button>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th colSpan="3"></th>
					<th>Cena celkem</th>
					<th colSpan="2">{sum} Kč</th>
				</tr>
			</tfoot>
		</Table>
	);
}

CostsInput.propTypes = {
	costs: PropTypes.array.isRequired,
	input: PropTypes.shape({
		itemTitle: PropTypes.string,
		itemCost: PropTypes.string
	}).isRequired,
	onAddCost: PropTypes.func.isRequired,
	onRemoveCost: PropTypes.func.isRequired,
};