import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'react-bootstrap';
import LabeledPassword from '../forms/LabeledPassword';


export default function ChangePasswordForm({
    oldPassword,
    newPassword,
    onPasswordChange,
    onSubmit
}) {
    return (
        <Form>
            <LabeledPassword 
                id='old-password'
                isRequired={true}
                label='Staré heslo'
                name='oldPassword'
                value={oldPassword}
                onChange={onPasswordChange}
            />

            <LabeledPassword 
                id='new-password'
                isRequired={true}
                label='Nové heslo'
                name='newPassword'
                value={newPassword}
                onChange={onPasswordChange}
            />

            <Button 
                className='pull-right'
                onClick={onSubmit}    
            >
                Změnit heslo
            </Button>
        </Form>
    )
};

ChangePasswordForm.props = {
    oldPassword: PropTypes.string.isRequired,
    newPassword: PropTypes.string.isRequired,
    onPasswordChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
};