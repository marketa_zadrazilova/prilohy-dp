import React from 'react';
import ChangePasswordForm from './ChangePasswordForm';
import { isRequired, minLength } from '../../utils/validation/ValidationRules';

export default class ChangePasswordFormContainer extends React.Component {
    state = {
        oldPassword: '',
        newPassword: ''
    };

    validationRules = {
        'oldPassword': [isRequired, minLength(5)],
        'newPassword': [isRequired, minLength(5)]
    };

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    onSubmit = () => {
        alert('Heslo bylo úspěšně změněno.');
    }

    render() {
        return (
            <ChangePasswordForm
                oldPassword={this.state.oldPassword}
                newPassword={this.state.newPassword}
                onSubmit={this.onSubmit}
                onPasswordChange={this.onChange}
            />
        );
    }
}