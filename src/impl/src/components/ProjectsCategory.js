import React from 'react';
import * as DataUtils from '../fakeit/DataUtils';
import PaginatedProjectsGrid from './PaginatedProjectsGrid';

const categoryName = "Nejnovější projekty";
const projects = DataUtils.getProjects();

export default class ProjectsCategory extends React.Component {
	render() {
		return (
			<div>
				<h3>{categoryName}</h3>

				<PaginatedProjectsGrid projects={projects} pageSize={3} />
			</div>
		);
	}
}