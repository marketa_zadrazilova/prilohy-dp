import React from 'react';
import PropTypes from 'prop-types';
import { Panel, Col, Glyphicon, Image } from 'react-bootstrap';
import { withRouter } from 'react-router';
import Progress from './Progress';
import { getStateLabel } from '../constants/ProjectStates';
import CategoriesList from './common/CategoriesList';


const wrapperStyle = {
	position: 'relative',
	width: '328px',
	height: '185px',
	marginBottom: '20px',
	overflow: 'hidden',
};

const overlayStyle = {
	position: 'absolute',
	bottom: 0,
	width: '100%',
	height: '30px',
	backgroundColor: '#fff',
	opacity: 0.7,
	padding: '5px',
	fontWeight: 'bold',
};

class ProjectTile extends React.Component {
	render() {
		const project = this.props.project;

		return (
			<Col md={4}>
				<Panel 
					className='project-tile'
					onClick={() => this.props.history.push('/projekty/' + project.id)}
				>
					<h3>{project.name}</h3>
					<span><Glyphicon glyph='map-marker'/>{project.location.title}</span>
					
					<div style={wrapperStyle}>
						<Image src={project.image_url} responsive/>

						<div style={overlayStyle}>{getStateLabel(project.projectState)}</div>
					</div>

					<p>
						{project.descriptionShort}
					</p>

					{ project.progress &&
						<Progress progress={project.progress} />
					}

					<CategoriesList categories={project.categories} />
				</Panel>
			</Col>
		);
	}
}

export default withRouter(ProjectTile);

ProjectTile.propTypes = {
	project: PropTypes.object.isRequired
};