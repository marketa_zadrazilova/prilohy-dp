import React from 'react';
import Dropzone from 'react-dropzone';
import { Col, Row, Image, Glyphicon, Button } from 'react-bootstrap';


export default class GalleryForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			default: null,
			files: []
		};

		this.handleImageDrop = this.handleImageDrop.bind(this);
		this.removeImage = this.removeImage.bind(this);
		this.setAsDefault = this.setAsDefault.bind(this);
	}

	handleImageDrop(acceptedFiles) {
		var files = this.state.files.slice();
		files = files.concat(acceptedFiles);

		console.log(files);

		this.setState({files});
	}

	removeImage(image) {
		const files = this.state.files.filter(item => item !== image);
		this.setState({files});
	}

	setAsDefault(image) {
		this.setState({default: image});
	}

	render() {
		const images = this.state.files.map((file, idx) => {
			console.log(file);
	
			const isDefault = this.state.default === file;

			return (
				<div className="galleryThumb" key={idx}>
					<Image src={file.preview} className="object-fit_cover" width="260px" height="180px" />
					<div className="overlay">
						<Button className="pull-right" onClick={()=>this.removeImage(file)}>
							<Glyphicon glyph="trash" />
						</Button>
						{!isDefault &&
							<Button className="pull-right" onClick={()=>this.setAsDefault(file)}>
								Nastavit jako úvodní
							</Button>
						}
					</div>
					{ isDefault &&
						<div className="default">
							Úvodní obrázek
						</div>
					}
				</div>
			);
		});

		return (
			<div>
				<Row>
					<Col md={12}>
						<Dropzone
							className="gallery-dropzone"
							multiple={true}
							accept="image/*"
							onDrop={this.handleImageDrop}
						>
							<p>Vložte obrázek přetažením, nebo klikněte pro výběr souboru.</p>
						</Dropzone>
					</Col>
				</Row>

				<Row>
					<Col md={12}>
						{images}
					</Col>
				</Row>
			</div>
		);
	}
}