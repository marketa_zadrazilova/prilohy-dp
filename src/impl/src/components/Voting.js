import React from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';


//todo predat info, jestli uz user hlasoval
export default class Voting extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			upVotes: 156,
			downVotes: 29,
			myVote: ''
		};

		this.upvote = this.upvote.bind(this);
		this.downvote = this.downvote.bind(this);
	}

	upvote() {
		if (this.state.myVote === '') {
			const upVotes = this.state.upVotes;
			this.setState({
				upVotes: upVotes + 1,
				myVote: 'up'
			});
		} else {
			alert('Už jste hlasoval.');
		}
	}

	downvote() {
		if (this.state.myVote === '') {
			const downVotes = this.state.downVotes;
			
			this.setState({
				downVotes: downVotes + 1,
				myVote: 'down'
			});
		} else {
			alert('Už jste hlasoval.');
		}
	}

	render() {
		const { upVotes, downVotes } = this.state;

		return (
			<div className='voting'>
				<p><strong>Co si myslíte o projektu?</strong></p>
				<Button 
					active={this.state.myVote === 'up'}
					onClick={this.upvote}
				>
					<Glyphicon glyph='thumbs-up' /> {upVotes}
				</Button> 
				<Button 
					active={this.state.myVote === 'down'}
					onClick={this.downvote}
				>
					<Glyphicon glyph='thumbs-down' /> {downVotes}
				</Button>
			</div>
		);
	}
}

Voting.propTypes = {
	votes: PropTypes.shape({
		upVotes: PropTypes.number,
		downVotes: PropTypes.number,
	})
}