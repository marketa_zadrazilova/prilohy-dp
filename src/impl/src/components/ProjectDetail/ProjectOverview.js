import React from 'react';
import PropTypes from 'prop-types';
import { Glyphicon, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AuthorInfo from './AuthorInfo';
import CategoriesList from '../common/CategoriesList';


export default function ProjectOverview(props) {
    const {
        author,
        project
    } = props;

    return (
        <div className="project-overview">
            <span>
                <AuthorInfo />
                <Glyphicon glyph="user" /> <Link to={{pathname: author.link}}>{author.name}</Link>
            </span>

            <span className="pull-right">
                <Glyphicon glyph="map-marker" /> {project.location.title}
            </span>

            <p>{project.descriptionShort}</p>
            
            <div className="">
                <Image 
                    src={project.image_url} 
                    rounded 
                    height="320"
                    width="100%"
                    className="object-fit_cover"
                />
            </div>

            <CategoriesList categories={project.categories} />
        </div>
    );
}

ProjectOverview.propTypes = {
    author: PropTypes.shape({
        link: PropTypes.string,
        name: PropTypes.string.isRequired
    }),
    project: PropTypes.shape({
        image_url: PropTypes.string.isRequired,
        descriptionShort: PropTypes.string.isRequired,
        location: PropTypes.object.isRequired,
        categories: PropTypes.arrayOf(PropTypes.string).isRequired
    })
};