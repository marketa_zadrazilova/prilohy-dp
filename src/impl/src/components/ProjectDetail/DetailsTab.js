import React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import CostsTable from '../costs/CostsTable';
import NewsList from '../news/NewsList';
import ProjectLocationMap from '../maps/ProjectLocationMap';

export default function DetailsTab(props) {
    const {
        project,
        costs,
        news
    } = props;

    return (
        <Row>
            <Col sm={7}>
                <Row>
                    <Col sm={12}>
                        <h3>Více o projektu</h3>
                        <p>{project.descriptionLong}</p>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <h3>Náklady</h3>
                        <CostsTable costs={costs} />
                    </Col>
                </Row>
            </Col>

            <Col sm={5}>
                <Row>
                    <Col sm={12}>
                        <h3>Novinky</h3>
                        <NewsList news={news} />
                    </Col>
                </Row>
                <Row>
                    <Col sm={12}>
                        <h3 id='location'>Umístění</h3>
                        <ProjectLocationMap location={project.location} />
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}

DetailsTab.propTypes = {
    project: PropTypes.shape({
        location: PropTypes.object,
        desrciptionLong: PropTypes.string,
    }),
    news: PropTypes.array,
    costs: PropTypes.array
};