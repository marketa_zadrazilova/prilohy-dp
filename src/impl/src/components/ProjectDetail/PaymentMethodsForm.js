import React from 'react';
import { Radio, FormGroup } from 'react-bootstrap';

export default function PaymentMethodsForm(props) {

    return (
        <div>
            <h4>Vyberte způsob platby</h4>
            
            <FormGroup>
                <h5>Platba kartou</h5>
                <Radio 
                    name="paymentMethod"
                    value="card"
                >
                    Visa, Master Card, Maestro
                </Radio>
                
                <h5>Online bankovní převod</h5>
                <Radio 
                    name="paymentMethod" 
                    value="mojebanka"
                >
                    Moje Banka
                </Radio>
                <Radio 
                    name="paymentMethod"
                    value="csob"
                >
                    ČSOB
                </Radio>
            </FormGroup>
        </div>
    );
}