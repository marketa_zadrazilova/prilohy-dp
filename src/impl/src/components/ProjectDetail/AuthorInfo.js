import React from 'react';
import PropTypes from 'prop-types';
import { Col, Image, Modal, Row } from 'react-bootstrap';

export default function AuthorInfo(props) {
    const profile = props.profile;

    return (
        <Modal show={false}>
            <Row>
                <Col md={5}>
                    <Image
                        src='/img/profile-w.svg'
                    />
                </Col>
                <Col md={7}>
                    <h2>{profile.name}</h2>
                    <h3>Emailová adresa</h3>
                    <p>{profile.email}</p>

                    <h3>O mě</h3>
                    <p>
                        {profile.about}
                    </p>
                </Col>
            </Row>
        </Modal>
    );
};

AuthorInfo.defaultProps = {
    profile: {
        about: "Lorem ipsum ....",
        email: "john.doe@email.com",
        imageUrl: "/img/profile-w.svg",
        name: "John Doe"
    }
};

AuthorInfo.props = {
    profile: PropTypes.shape({
        about: PropTypes.string,
        email: PropTypes.string.isRequired,
        imageUrl: PropTypes.string,
        name: PropTypes.string.isRequired,
    }).isRequired,
};