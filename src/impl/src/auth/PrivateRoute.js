import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import UserSession from '../utils/UserSession';


const PrivateRoute = ({component: Component, ...rest}) => (
	<Route {...rest} render={props => (
		UserSession.isAuthenticated() ? (
			<Component {...props} />
		):(
			<Redirect to={{
				pathname:"/prihlaseni",
				state: {from: props.location}
			}} />
		)
	)} />
);

export default PrivateRoute;