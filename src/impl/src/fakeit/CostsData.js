var costs = {
	byIds: {
		1: [
			{
				name: 'piáno',
				value: 10000
			},
			{
				name: 'uzamykatelná skříň',
				value: 5000
			},
			{
				name: 'stolička k piánu',
				value: 100
			},
			{
				name: 'stěhování',
				value: 500
			}
		],
		2: [
			{
				name: 'materiál na výrobu informační tabule',
				value: 2000,
			},
			{
				name: 'barva na natření návěstidla',
				value: 1500,
			}
		],
		3: [
			{
				name: '20ks velkých květináčů',
				value: 40000,
			},
			{
				name: 'zemina',
				value: 50000,
			},
			{
				name: 'zahradnické náčiní',
				value: 25000,
			}
		],
		4: [
			{
				name: 'položka č. 1',
				value: 550
			},
			{
				name: 'položka č. 2',
				value: 1250
			},
			{
				name: 'položka č. 3',
				value: 760
			}
		]
	},
	allIds: [1, 2, 3, 4]
};

export function getCostsById(id) {
	if (!costs.allIds.includes(id)) {
		throw new Error(`Costs with id=${id} does not exist!`);
	}

	return costs.byIds[id];
}

export function addCosts(costs) {
	const newId = costs.allIds[costs.allIds.length - 1] + 1;

	costs.byIds = {
		...costs.byIds,
		[newId]: costs
	};

	costs.allIds = [
		...costs.allIds,
		newId
	];
}