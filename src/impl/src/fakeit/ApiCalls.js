import * as dataUtils from './DataUtils';


/* --- COMMENTS ---------------------------------------------------------------- */

export function getComments(projectId) {
	return new Promise((resolve, reject) => {
		resolve(dataUtils.getComments());
	});
}

export function createComment(projectId, comment) {
	return new Promise((resolve, reject) => {
		resolve(dataUtils.addComment(comment));
	});
}