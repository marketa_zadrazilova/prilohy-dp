import * as dateUtils from '../utils/DateUtils';

const PUBLIC_STATES = ['STUDY', 'FEASIBLE','IN_FUNDING', 'IN_REALIZATION'];

var projects = {
	byId: {
		1: {
			id: 1,
			name: "Piáno v parku",
			categories: ["art-culture", "music"],
			descriptionShort: "Pojďme rozeznít městké sady hudbou!",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "IN_FUNDING",
			author: 1,
			location: {
				title: "Altánek v Palackého sadech",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			progress: {
				pledged: 200,
				pledgedByCity: 1000,
				goal: 10000
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/piano.jpg",
			comments: [],
			costs: [
				{
					title: 'piáno',
					unitCount: 1,
					unitCost: 10000
				},
				{
					title: 'uzamykatelná skříň',
					unitCount: 1,
					unitCost: 5000
				},
				{
					title: 'stolička k piánu',
					unitCount: 1,
					unitCost: 100
				},
				{
					title: 'stěhování',
					unitCount: 1,
					unitCost: 500
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		2: {
			id: 2,
			name: "Obnova návěstidla",
			categories: ["public-space"],
			descriptionShort: "Chceme obnovit historické návěstidlo v centru obce.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "STUDY",
			author: 4,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/navestidlo.jpg",
			comments: [],
			costs: [
				{
					title: 'materiál na výrobu informační tabule',
					unitCount: 1,					
					unitCost: 2000,
				},
				{
					title: 'barva na natření návěstidla',
					unitCount: 1,					
					unitCost: 1500,
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		3: {
			id: 3,
			name: "Komunitní zahrádka",
			categories: ["community", "public-space"],
			descriptionShort: "Vlastní zelenina a ovoce chutná nejlépe. Díky komunitní zahrádce si ji můžete dopřát i vy.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "IN_FUNDING",
			author: 3,
			location: {
				title: "Velké náměstí",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			progress: {
				pledged: 0,
				goal: 150000
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/zahradka.jpg",
			comments: [],
			costs: [
				{
					title: '20ks velkých květináčů',
					unitCount: 1,
					unitCost: 40000,
				},
				{
					title: 'zemina',
					unitCount: 1,					
					unitCost: 50000,
				},
				{
					title: 'zahradnické náčiní',
					unitCount: 1,					
					unitCost: 25000,
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"			
		},
		4: {
			id: 4,
			name: "Šachové odpoledne",
			categories: ["sport"],
			descriptionShort: "Na nábřeží proběhne celodenní utání v šachách pro širokou veřejnost.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "IN_REALIZATION",
			author: 2,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			progress: {
				pledged: 67000,
				goal: 67000
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/sachy.jpg",
			comments: [],
			costs: [
				{
					title: 'položka č. 1',
					unitCount: 1,
					unitCost: 550
				},
				{
					title: 'položka č. 2',
					unitCount: 1,
					unitCost: 1250
				},
				{
					title: 'položka č. 3',
					unitCount: 1,
					unitCost: 760
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		5: {
			id: 5,
			name: "Sousedské slavnosti",
			categories: ["community"],
			descriptionShort: "Chceme uspořádat sousedské slavnosti na několika místech ve městě s bohatým prorogramem.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "FEASIBLE",
			author: 4,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/slavnosti.jpg",
			comments: [],
			costs: [
				{
					title: 'položka č. 1',
					unitCount: 1,
					unitCost: 550
				},
				{
					title: 'položka č. 2',
					unitCount: 1,
					unitCost: 1250
				},
				{
					title: 'položka č. 3',
					unitCount: 1,
					unitCost: 760
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		6: {	
			id: 6,
			name: "Malování na chodník",
			categories: ["children"],
			descriptionShort: "Uspořádáme malování na chodník pro děti před MŠ Sborovská.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "IN_FUNDING",
			author: 4,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			progress: {
				pledged: 200,
				goal: 10000
			},
			reactions: {
				upVotes: 156,
				downVotes: 17
			},
			image_url: "/img/streetart.jpg",
			comments: [],
			costs: [
				{
					title: 'položka č. 1',
					unitCount: 1,
					unitCost: 550
				},
				{
					title: 'položka č. 2',
					unitCount: 1,
					unitCost: 1250
				},
				{
					title: 'položka č. 3',
					unitCount: 1,
					unitCost: 760
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		7: {
			id: 7,
			name: "New project",
			categories: ["children"],
			descriptionShort: "Uspořádáme malování na chodník pro děti před MŠ Sborovská.",
			descriptionLong: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque egestas ligula risus, id sagittis nisl ornare eget. Morbi nisi orci, vehicula tincidunt magna at, mattis cursus enim. Nam lacus felis, placerat non dictum eu, ultricies sed mi. Morbi interdum iaculis viverra. Morbi efficitur lobortis nisi vel blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu massa quis turpis bibendum lobortis. Curabitur sodales ipsum non lobortis ultrices. Ut feugiat erat eget sollicitudin vulputate. Pellentesque tellus lectus, aliquet eu risus sit amet, sollicitudin dapibus nunc. Aliquam eget diam ut odio ultrices sodales. In cursus volutpat tellus at dapibus. Ut consequat tortor quis risus finibus pretium. Donec viverra est eget arcu interdum, et tincidunt neque feugiat.",
			projectState: "NEW",
			author: 1,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			image_url: "/img/streetart.jpg",
			costs: [
				{
					title: 'položka č. 1',
					unitCount: 1,
					unitCost: 550
				},
				{
					title: 'položka č. 2',
					unitCount: 1,
					unitCost: 1250
				},
				{
					title: 'položka č. 3',
					unitCount: 1,
					unitCost: 760
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		},
		8: {
			id: 8,
			name: 'My Awaiting Project',
			categories: ['other'],
			descriptionShort: 'msmdkma',
			descriptionLong: 'masmkfkeffkemkama',
			projectState: 'AWAITING',
			author: 4,
			location: {
				title: "Nádražní",
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			image_url: '/img/streetart.jpg',
			costs: [
				{
					title: 'položka č. 1',
					unitCount: 1,
					unitCost: 550
				},
				{
					title: 'položka č. 2',
					unitCount: 1,
					unitCost: 1250
				},
				{
					title: 'položka č. 3',
					unitCount: 1,
					unitCost: 760
				}
			],
			createdAt: "2017-03-04T19:11:07+00:00",
			updatedAt: "2017-03-04T19:11:07+00:00"
		}
	},
	allIds: [1, 2, 3, 4, 5, 6 , 7, 8]
};

var users = {
	byId: {
		1: {
			id: 1,
			name: 'Karel Novák',
			email: 'k.novak@seznam.cz',
			about: ''
		},
		2: {
			id: 2,
			name: 'Jana Novotná',
			email: 'j.novotna@email.com',
			about: ''
		},
		3: {
			id: 3,
			name: 'František Dobrota',
			email: 'dobrota.f@gmail.com',
			about: ''
		},
		4: {
			id: 4,
			name: 'Josef Holý',
			email: 'j.holy@email.cz',
			about: ''
		}
	},
	allIds: [1, 2, 3, 4],
};

export function getUserById(id) {
	if (! users.allIds.includes(id)) {
		throw new Error(`User with id=${id} does not exist!`);
	}

	return users.byId[id];
}

export function updateUser(id, data) {
	console.log('user update');
	if (! users.allIds.includes(id)) {
		throw new Error(`User with id=${id} does not exist!`);
	}

	users = {
		...users,
		byId: {
			...users.byId,
			[id]: data
		}
	};

	console.log(users);
}

var comments = [
	{
		id: 1,
		author: {
			name: "Markéta Zadražilová",
			image_url: "/img/profile-w.svg",
			url: "/users/1"
		},
		text: "To vypadá super!",
		created_at: "2017-03-04T19:11:07+00:00"
	},
	{
		id: 2,
		author: {
			name: "Karel Nový",
			image_url: "/img/profile-m.png",
			url: "/users/2"
		},
		text: "Taky jsem napsal komentář.",
		created_at: "2017-03-05T12:11:07+00:00"
	}
];

var lastCommentId = 2;

var news = [
	{
	    "id": 2,
	    "created_at": "2017-03-17T12:11:07+00:00",
	    "title": "Máme piáno!",
	    "text": "Pan Soukup nám přispěl piánem. Děkujeme!",
	},
	{
	    "id": 1,
	    "created_at": "2017-03-05T12:11:07+00:00",
	    "title": "Projekt spuštěn",
	    "text": "Text novinky",
	}
];

var gallery = [
	{
		src: '/img/gallery-content/1.jpg'
	},
	{
		src: '/img/gallery-content/2.jpg'
	},
	{
		src: '/img/gallery-content/3.jpg'
	},
	{
		src: '/img/gallery-content/4.jpg'
	},
	{
		src: '/img/gallery-content/5.jpg'
	},
	{
		src: '/img/gallery-content/6.jpg'
	},
	{
		src: '/img/gallery-content/7.jpg'
	},
];

export function getProjects() {
	return projects.allIds.map(id => projects.byId[id]);
}

export function getPublicProjects() {
	return getProjects().filter(project => PUBLIC_STATES.includes(project.projectState))
}

//TODO: immutable
export function addProject(project) {
	console.log('Adding project', project);
	const id = projects.allIds[projects.allIds.length-1] + 1;

	projects.byId[id] = {
		...project,
		id
	};
	projects.allIds.push(id);
}

export function updateProject(project) {
	projects.byId[project.id] = {
		...project
	};
}

export function saveProject(newProject) {
	let project = {...newProject};

	if (!project.id) {
		const id = projects.allIds[projects.allIds.length-1] + 1;
		project.id = id;
		project.createdAt = dateUtils.getFormatedTime();

		projects.allIds.push(id);
	}

	project.updatedAt = dateUtils.getFormatedTime();

	projects.byId[project.id] = {
		...project
	};

	console.log('Updated project records', projects);
	console.log('Project updated', project);

	return project;
}

export function getProject(id) {
	if (!projects.allIds.includes(id)) {
		throw new Error(`Project with id=${id} does not exist!`);
	}

	return projects.byId[id];
}

export function getMyProjects() {
	return getProjects().filter(project => project.author === 4);
}

export function getComments() {
	return comments;
}

export function addComment(comment) {
	const newComment = {
		id: ++lastCommentId,
		author: {
			name: "Josef Holý",
			image_url: "/img/profile-m.png",
			url: "/users/4"
		},
		text: comment.text,
		created_at: dateUtils.getFormatedTime()
	};

	comments.push(newComment);
}

export function getNews() {
	return news;
}

export function getGallery() {
	return gallery;
}