import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

//REDUX
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import mainReducer from './reducers';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import Layout from './pages/Layout';
import PrivateRoute from './auth/PrivateRoute';
import Home from './pages/Home';
import About from './pages/About';
import LogInContainer from './containers/LogInContainer';
import SignUpContainer from './containers/SignUpContainer';
import ProjectsListContainer from './containers/ProjectsListContainer';
import ProjectDetailContainer from './containers/ProjectDetailContainer';
import NewProjectContainer from './containers/NewProjectContainer';
import MyProjectsContainer from './containers/MyProjectsContainer';
import ProjectDashboardContainer from './containers/ProjectDashboardContainer';
import MyProfileContainer from './containers/MyProfileContainer';
import SearchResultContainer from './containers/SearchResultContainer';
import PageNotFound from './pages/PageNotFound';

/* CSS ----------------------------------------------- */
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.

const loggerMiddleware = createLogger();
let store = createStore(mainReducer, applyMiddleware(thunkMiddleware, loggerMiddleware));


ReactDOM.render((
	<Provider store={store}>
		<Router>
			<Layout>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route path="/prihlaseni" component={LogInContainer} />
					<Route path="/registrace" component={SignUpContainer} />
					<Route path="/projekty/:id" component={ProjectDetailContainer} />
					<Route exact path="/projekty" component={ProjectsListContainer} />
					<PrivateRoute path="/novy-projekt" component={NewProjectContainer} />
					<PrivateRoute path="/autor/muj-profil" component={MyProfileContainer} />
					<PrivateRoute exact path="/autor/projekty" component={MyProjectsContainer} />
					<PrivateRoute path="/autor/projekty/:id" component={ProjectDashboardContainer} />
					<Route path="/vyhledavani" component={SearchResultContainer} />
					<Route path="/jak-to-funguje" component={About} />
					<Route component={PageNotFound} />
				</Switch>
			</Layout>
		</Router>
	</Provider>
  ),
  document.getElementById('root')
);