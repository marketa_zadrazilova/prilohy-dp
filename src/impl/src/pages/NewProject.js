import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Row, Col, Nav, NavItem } from 'react-bootstrap'; 
import CostsInput from '../components/costs/CostsInput';
import ProjectDetailsForm from '../components/ProjectDetailsForm';
import SetLocationMap from '../components/maps/SetLocationMap';
import LabeledInput from '../components/forms/LabeledInput';
import GalleryForm from '../components/GalleryForm';

import FormPaneLayout from '../components/layouts/FormPaneLayout';
import { Button } from 'react-bootstrap';


export default function NewProject({
	activeTab,
	handleSelect,
	onSubmitProject,
	form,
	onItemChange,
	onCategoriesChange,
	...props
}) {
	return (
		<div>
			<h2>Vytvořit nový projekt</h2>

			<Tab.Container 
				activeKey={activeTab} 
				id="new-project"
				onSelect={handleSelect}
			>
				<Row className="clearfix">
					<Col md={3}>
						<Nav bsStyle="pills" stacked>
							<NavItem eventKey={1}>
								O projektu
							</NavItem>
							<NavItem eventKey={2}>
								Umístění
							</NavItem>
							<NavItem eventKey={3}>
								Náklady
							</NavItem>
							<NavItem eventKey={4}>
								Galerie
							</NavItem>
							<NavItem eventKey={5}>
								Dokončit projekt
							</NavItem>
						</Nav>
					</Col>
					<Col md={9}>
						<Tab.Content>
							<Tab.Pane eventKey={1}>
								<FormPaneLayout
									description="V této části vyplňte základní informace o Vašem projektu."
									isFirst={true}
									onSave={props.onSave}
									onPrev={props.onPrev}
									onNext={props.onNext}
									title="Info o projektu"
								>								
									<ProjectDetailsForm 
										projectDetails={form} 
										onItemChange={onItemChange} 
										onCategoriesChange={onCategoriesChange}
									/>
								</FormPaneLayout>
							</Tab.Pane>
							<Tab.Pane eventKey={2}>
								<FormPaneLayout 
									description="Kliknutím myši umístěte Váš projekt do mapy. Vybranou lokaci změníte přetažením značky. Pro odstranění klikněte na značku pravým tlačítkem myši."
									onPrev={props.onPrev}
									onNext={props.onNext}
									title="Umístění"
									{...props}
								>
									<Row className="location-pane">
										<Col sm={12}>
											<SetLocationMap />
										</Col>
										<Col sm={12}>
											<LabeledInput 
												id="location-info"
												label="Upřesnění místa"
												type="text"
												name="title"
												value={form.location.title}
												onChange={onItemChange}
											/>
										</Col>
									</Row>
								</FormPaneLayout>
							</Tab.Pane>
							<Tab.Pane eventKey={3}>
								<FormPaneLayout
									description="Do tabulky zadejte předpokládané náklady na realizaci vašeho projektu. Uveďte jak pořizovací náklady, tak náklady na údržbu po dobu běhu projektu."
									onPrev={props.onPrev}
									onNext={props.onNext}
									title="Odhad nákladů"
									{...props}
								>
									<CostsInput 
										costs={form.costs}
										input={props.costsInput}
										onInputChange={onItemChange}
										onAddCost={props.onAddCost}
										onRemoveCost={props.onRemoveCost}
									/>
								</FormPaneLayout>
							</Tab.Pane>
							<Tab.Pane eventKey={4}>
								<FormPaneLayout 
									description="V této části můžete vložit fotky a obrázky, které pomohou přiblížit Váš projekt ostatním. Označte alespoň jednu fotku jako úvodní."
									onPrev={props.onPrev}
									onNext={props.onNext}
									title="Galerie"
									{...props}
								>
									<GalleryForm />
								</FormPaneLayout>
							</Tab.Pane>
							<Tab.Pane eventKey={5}>
								<FormPaneLayout
									description="Nyní můžete dokončit projekt. Kliknutím na tlačítko 'Vytvořit projekt' souhlasíte s podmínkami CityFunderu."
									isLast={true}
									title="Dokončit projekt"
									{...props}
								>
									<Button 
										onClick={onSubmitProject}
										bsStyle='success'
									>
										Vytvořit projekt
									</Button>
								</FormPaneLayout>
							</Tab.Pane>
						</Tab.Content>
					</Col>
				</Row>
			</Tab.Container>
		</div>
	);
}

NewProject.propTypes = {
	form: PropTypes.object.isRequired,
	onItemChange: PropTypes.func.isRequired,
	activeTab: PropTypes.number.isRequired,
	handleSelect: PropTypes.func.isRequired,
	onSave: PropTypes.func.isRequired,
	onPrev: PropTypes.func.isRequired,
	onNext: PropTypes.func.isRequired,
}