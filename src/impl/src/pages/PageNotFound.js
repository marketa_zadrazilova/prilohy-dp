import React from 'react';


export default function PageNotFound(props) {
	return (
		<div>
			<h2>Stránka nenalezena</h2>
			<p>Požadovaná stránka bohužel nebyla nalezena</p>
		</div>
	);
}