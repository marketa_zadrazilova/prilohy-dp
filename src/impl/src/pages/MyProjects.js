import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Glyphicon, Table } from 'react-bootstrap';
import DateFormatter from '../components/common/DateFormatter';
import { getStateLabel } from '../constants/ProjectStates';
//import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';


export default function MyProjects({projects}) {
	const rows = projects.map(item => (
			<tr key={item.id}>
				<td>{item.name}</td>
				<td>{getStateLabel(item.projectState)}</td>
				<td><DateFormatter date={item.createdAt} /></td>
				<td><DateFormatter date={item.updatedAt} /></td>
				<td>
					<Link className='btn btn-default' to={'/autor/projekty/' + item.id}>
						<Glyphicon glyph='wrench'/> Správa projektu
					</Link>
				</td>
				<td>
					<Link className='btn btn-default' to={'/projekty/' + item.id}>
						<Glyphicon glyph="search"/> Zobrazit náhled projektu
					</Link>
				</td>
			</tr>
		)
	);

	return (
		<div>
			<h2>Moje projekty</h2>

			{rows.length > 0 ? (
					<Table>
						<thead>
							<tr>
								<th>Název</th>
								<th>Stav</th>
								<th>Datum vytvoření</th>
								<th>Datum poslední změny</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{rows}
						</tbody>
					</Table>
				) : (
					<p>Ještě jste nevytvořil žádný projekt.</p>
				)
			}

			{/*
			<BootstrapTable data={projects} striped hover>
				<TableHeaderColumn
					isKey
					dataField='id'
					hidden={true}
				>Název</TableHeaderColumn>
				<TableHeaderColumn dataField='name'>Název</TableHeaderColumn>
				<TableHeaderColumn dataField='projectState'>Stav</TableHeaderColumn>
				<TableHeaderColumn dataField='createdAt'>Datum vytvoření</TableHeaderColumn>
				<TableHeaderColumn dataField='updatedAt'>Datum poslední změny</TableHeaderColumn>
				<TableHeaderColumn></TableHeaderColumn>
				<TableHeaderColumn></TableHeaderColumn>
			</BootstrapTable>
			*/}
		</div>
	);
}

MyProjects.propTypes = {
	projects: PropTypes.array
};