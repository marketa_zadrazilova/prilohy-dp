import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button, Form } from 'react-bootstrap';
import LabeledInput from '../components/forms/LabeledInput';
import LabeledPassword from '../components/forms/LabeledPassword';


export default function LogInForm({
	form,
	onInputChange,
	onFormSubmit,
	validation,
}) {
	return (
		<Form className="form-login">
			<h2>Přihlášení</h2>

			<LabeledInput
				id="login-email"
				isRequired={true}
				label="Emailová adresa"
				name="email"
				onChange={onInputChange}
				type="email"
				validation={validation.email}
				value={form.email}
			/>

			<LabeledPassword
				id="login-passwd"
				isRequired={true}
				label="Heslo"
				name="password"
				onChange={onInputChange}
				validation={validation.password}
				value={form.password}
			/>
			<a>Zapomněli jste heslo?</a>

			<br/>
			<Button bsStyle="success" bsSize="large" block onClick={onFormSubmit}>
				Přihlásit se
			</Button>

			<br/>

			<p>Ještě u nás nemáte účet? <Link to="/registrace">Přejít na registraci.</Link></p>
		</Form>
	);
}

LogInForm.propTypes = {
	form: PropTypes.object.isRequired,
	onInputChange: PropTypes.func.isRequired,
	onFormSubmit: PropTypes.func.isRequired,
	validation: PropTypes.object,
};