import React from 'react';
import PropTypes from 'prop-types';
import FilterableProjectsList from '../components/FilterableProjectsList';


const ProjectsList = ({projects}) => (
	<div>
		<h2>Projekty</h2>
		<FilterableProjectsList projects={projects} />
	</div>
)

ProjectsList.propTypes = {
	projects: PropTypes.array.isRequired
};

export default ProjectsList;