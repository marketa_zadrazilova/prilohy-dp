import React from 'react';
import FilterableProjectsList from '../components/FilterableProjectsList';


export default function SearchResult({query, projects}) {	
	return (
		<div>
			<h2>Výsledky vyhledávání</h2>

			<p>Pro vyhledávání "{query}" jsme nalezli {projects.length} projektů.</p>
		
			<FilterableProjectsList projects={projects} />
		</div>
	);
}