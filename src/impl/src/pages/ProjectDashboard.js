import React from 'react';
import { Link } from 'react-router-dom';
import StatePanel from '../components/statePanels/projectDashboard/StatePanel';
import StateTabs from '../components/stateTabs/StateTabs';


export default function ProjectDashboard({project, news, costs}) {
	return (
		<div>
			<h2><Link to="/autor/projekty">Moje projekty</Link> > {project.name}</h2>

			<StatePanel project={project} />

			<hr />

			<StateTabs project={project} costs={costs} news={news} mode="UPDATE" />
		</div>
	);
}