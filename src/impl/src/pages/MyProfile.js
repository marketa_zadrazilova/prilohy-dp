import React from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Panel, Row } from 'react-bootstrap';
import LabeledInput from '../components/forms/LabeledInput';
import LabeledTextarea from '../components/forms/LabeledTextarea';
import UpdatableImage from '../components/UpdatableImage';
import ChangePasswordFormContainer from '../components/MyProfile/ChangePasswordFormContainer';

export default function MyProfile({
	profile,
	onProfileChange,
	onProfileUpdate,
	passwords,
	onPasswordsChanged
}) {
	return (
		<div>
			<h2>Můj profil</h2>

			<Row>
				<Col md={6}>
					<Panel className="profile-details">

						<Row>
							<Col md={4}>
								<UpdatableImage />
							</Col>

							<Col md={8}>
								<LabeledInput 
									id='edit-profile-name'
									isRequired={true}
									label='Jméno a příjmení'
									name='name'
									type='text'
									value={profile.name}
									onChange={onProfileChange}
								/>

								<LabeledInput 
									id='edit-profile-email'
									isRequired={true}
									label='Email'
									type='email'
									name='email'
									value={profile.email}
									onChange={onProfileChange}
								/>
							</Col>
						</Row>

						<LabeledTextarea 
							id='edit-profile-about'
							label='Pár slov o mně'
							name='about'
							value={profile.about}
							onChange={onProfileChange}
						/>

						<Button 
							className='pull-right'
							onClick={onProfileUpdate}
						>
							Uložit změny
						</Button>
					</Panel>
				</Col>
				<Col md={6}>
					<Panel className="password-change">
						<h3>Změna hesla</h3>
						<ChangePasswordFormContainer />
					</Panel>
				</Col>
			</Row>
		</div>
	);
}

MyProfile.propTypes = {
	profile: PropTypes.shape({
		about: PropTypes.string,
		email: PropTypes.string.isRequired,
		imageUrl: PropTypes.string,
		name: PropTypes.string.isRequired,
	}).isRequired,
	passwords: PropTypes.object.isRequired,
	onProfileChange: PropTypes.func.isRequired,
	onProfileUpdate: PropTypes.func.isRequired,
	onPasswordsChanged: PropTypes.func.isRequired,
};