import React from 'react';
import { Carousel, Col, Row } from 'react-bootstrap';
import AllProjectsMap from '../components/maps/AllProjectsMap';
import ProjectsCategory from '../components/ProjectsCategory';


export default class Home extends React.Component {
	render () {
		return (
			<div>
				<Row>
					<Carousel>
						<Carousel.Item>
							<img width={1170} height={300} alt="900x500" src="/img/carousel/carousel.png"/>
							<Carousel.Caption>
								<h3>Zde budou slidy s vysvětlením principů CityFunderu</h3>
								<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img width={1170} height={300} alt="900x500" src="/img/carousel/carousel.png"/>
							<Carousel.Caption>
								<h3>Případně novinky</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</Carousel.Caption>
						</Carousel.Item>
						<Carousel.Item>
							<img width={1170} height={300} alt="900x500" src="/img/carousel/carousel.png"/>
							<Carousel.Caption>
								<h3>Či soutěže vyhlášené městem ...</h3>
								<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
							</Carousel.Caption>
						</Carousel.Item>
					</Carousel>
				</Row>
				<Row>
					<Col md={6}>
						<div className="about-cityfunder">
							<h3>City Funder</h3>
							<p>
								CityFunder je webová crowdfundingová platforma umožňující lidem prezentovat projekty ovlivňující veřejný prostor nebo kulturu, získat prostředky na jejich realizování a získat podporu města při jejich realizaci.
							</p>

							<h4>Prostřednictvím CityFunderu mohou občané města:</h4>
							<ul>
								<li>prezentovat své projekty</li>
								<li>dostat zpětnou vazbu od vedení města, úřadů a spoluobčanů</li>
								<li>získat prostředky na realizaci projektu</li>
								<li>získat podporu při shánění potřebných povolení přímo od města</li>
							</ul>

							<h4>Hlavní cíle projektu</h4>
							<ul>
								<li>aktivizace občanské společnosti</li>
								<li>vybudování vztahu mezi občany a městem</li>
								<li>zpestření veřejného prostoru a kultury ve městě</li>
							</ul>
						</div>
					</Col>
					<Col md={6}>
						<h3>Mapa projektů</h3>
						<AllProjectsMap />
					</Col>
				</Row>
				<Row>
					<Col md={12}>
						<ProjectsCategory />
					</Col>
				</Row>
			</div>
		);
	}
}