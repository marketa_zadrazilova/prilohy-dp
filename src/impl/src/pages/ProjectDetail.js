import React from 'react';
import CommentsContainer from '../containers/CommentsContainer';
import { Link } from 'react-router-dom';
import { Col, Row, Tabs, Tab  } from 'react-bootstrap';
import StatePanel from '../components/statePanels/projectDetail/StatePanel';
import Voting from '../components/Voting';
import Gallery from '../components/Gallery';
import Contributions from '../components/Contributions';
import StudyResultsTable from '../components/StudyResultsTable';
import DetailsTab from '../components/ProjectDetail/DetailsTab';
import ProjectOverview from '../components/ProjectDetail/ProjectOverview';


export default function ProjectDetail({
	author,
	project, 
	news, 
	gallery,
	costs,
	tabKey,
	onTabSelect,
}) {
		return (
			<div>
				<Row>
					<Col md={12}>
						<h2><Link to="/projekty">Projekty</Link> > {project.name}</h2>
					</Col>
				</Row>
				<Row>
					<Col md={7}>
						<ProjectOverview 
							author={author} 
							project={project} 
						/>
					</Col>
					<Col md={5}>
						<StatePanel project={project} />
					</Col>
				</Row>

				<Row>
					<Col md={12}>
						<Tabs
							activeKey={tabKey}
							id="project-tabs"
							onSelect={onTabSelect}
						>
							<Tab 
								eventKey="details"
								title="Více o projektu"
							>
								<DetailsTab
									project={project}
									news={news}
									costs={costs}
								/>
							</Tab>
							{ project.projectState !== 'STUDY' &&
								<Tab
									eventKey="studyResults"
									title="Vyjádření města"
								>
									<h3>Výsledky studie proveditelnosti</h3>
									<StudyResultsTable />
								</Tab>
							}
							{project.projectState === 'IN_FUNDING' &&
								<Tab 
									eventKey="contributions"
									title="Jak se mohu zapojit?"
								>
									<h3>Jak se mohu zapojit?</h3>
									<Contributions costs={costs} />
								</Tab>
							}
							<Tab
								eventKey="gallery"
								title="Galerie"
							>
								<Gallery gallery={gallery} />
							</Tab>
							<Tab 
								eventKey="comments"
								title="Komentáře"
							>
								<CommentsContainer />
							</Tab>
						</Tabs>
					</Col>
				</Row>
			</div>
		);
}