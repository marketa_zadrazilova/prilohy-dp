import React from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import HeaderContainer from '../containers/HeaderContainer';
import Footer from '../components/layout/Footer';

export default class Layout extends React.Component {
	render() {
		return (
			<div>
				<HeaderContainer />
				<Grid>
					<Row>
						<Col md={12}>
							{this.props.children}
						</Col>
					</Row>
				</Grid>
				<Footer />
			</div>
		);
	}
}