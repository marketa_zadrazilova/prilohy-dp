import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form } from 'react-bootstrap';
import LabeledInput from '../components/forms/LabeledInput';
import LabeledPassword from '../components/forms/LabeledPassword';
import ConditionsModal from '../components/modals/ConditionsModal';


export default function SignUpForm({
	form,
	onNameChanged,
	onEmailChanged,
	onPasswordChanged,
	onFormSubmitted,
	onInputChange,
	validation,
}) {
	return (
		<Form className="form-signup">
			<h2>Registrace</h2>

			<p>Už jste zaregistrován? <Link to="/prihlaseni">Přejít na přihlášení.</Link></p>

			<br/>

			<LabeledInput
				id="reg-first-name"
				isRequired={true}
				label="Jméno a příjmení"
				name="name"
				onChange={onInputChange}
				type="text"
				validation={validation.name}
				value={form.name}
			/>

			<LabeledInput
				id="reg-email"
				isRequired={true}
				label="Emailová adresa"
				name="email"
				onChange={onInputChange}
				type="email"
				validation={validation.email}
				value={form.email}
			/>

			<LabeledPassword
				id="reg-password"
				isRequired={true}
				label="Heslo"
				name="password"
				onChange={onInputChange}
				validation={validation.password}
				value={form.password}
			/>

			<ConditionsModal />
			
			<Button bsStyle="primary" bsSize="large" block onClick={onFormSubmitted}>
				Zaregistrovat se
			</Button>
		</Form>
	);
}