export function getSumOfCosts(costs) {
	var sum = 0;

	for (var cost of costs) {
		sum += Number(cost.unitCost) * Number(cost.unitCount);
	}

	return sum;
}