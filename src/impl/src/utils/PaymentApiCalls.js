import axios from 'axios';


export function authenticate() {
	axios({
		method: 'post',
		url:'https://secure.payu.com/pl/standard/user/oauth/authorize',
		data: 'grant_type=client_credentials&client_id=145227&client_secret=12f071174cb7eb79d4aac5bc2f07563f'
	}).then(response => {
		console.log(response);
	}).catch(error => {
		console.log(error);
	});
}

export function createPayment() {
	axios({
		method: 'post',
		url: 'https://secure.snd.payu.com/api/v2_1/orders',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer e630cdb1-2bd2-4e66-ba37-0166f5ce976d'
		},
		data: {
			notifyUrl: "https://your.eshop.com/notify",
		    customerIp: "127.0.0.1",
		    merchantPosId: "145227",
		    description: "RTV market",
		    currencyCode: "PLN",
		    totalAmount: "100",
		    buyer: {
		        "email": "john.doe@example.com",
		        "phone": "654111654",
		        "firstName": "John",
		        "lastName": "Doe",
		        "language": "en"
		    },
		    settings:{
		        "invoiceDisabled":"true"
		    },
		    products: [
		        {
		            "name": "Wireless Mouse for Laptop",
		            "unitPrice": "100",
		            "quantity": "1"
		        }
		    ]
		},
	}).then(response => {
		console.log(response);
	}).catch(error => {
		console.log(error);
	});
}