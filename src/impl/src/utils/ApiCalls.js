import axios from 'axios';

const instance = axios.create({
	baseURL: "http://private-818b1-cityfunder.apiary-mock.com"
});

export function setAuthToken(token) {
	if (token) {
		instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
	} else {
		delete instance.defaults.headers.common['Authorization'];
	}
}

export function getAllProjects() {
	instance.get('/projects')
		.then((response) => {
			console.log(response);
		}).catch((error) => {
			console.log(error);
		});
}

/* --- AUTH -------------------------------------------------------------------- */

//TODO: body requestu
//TODO: celkově dodělat
export function authenticate() {
	return new Promise((resolve, reject) => {
		instance.post('/auth/token', {}).then(response => {
			resolve();
		}).catch(error => {
			reject(error.response ? error.response.data : error.message);
		});
	});
}

/* --- COMMENTS ---------------------------------------------------------------- */

export function getComments(projectId) {
	return new Promise((resolve, reject) => {
		instance.get('/projects/' + projectId + '/comments').then(response => {
			resolve(response.data);
		}).catch(error => {
			reject(error.response ? error.response.data : error.message);
		});
	});
}

export function createComment(projectId, comment) {
	return new Promise((resolve, reject) => {
		instance.post('/projects/' + projectId + '/comments', comment).then(response => {
			console.debug(response);
			resolve();
		}).catch(error => {
			reject(error.response ? error.response.data : error.message);
		});
	});
}
