export const minLength = (minLength) => {
    return (value) => {
        if (value.length < minLength) {
            return `Toto pole musí být dlouhé alespoň ${minLength} znaků.`;
        }
        return null;
    };
};

export const maxLength = (maxLength) => {
    return (value) => {
        if (value.length > maxLength) {
            return `Toto pole musí být dlouhé maximálně ${maxLength} znaků.`;
        }
        return null;
    };
}

export const isRequired = (value) => {
    if (!value || value.length < 1 || value.size === 0) {
        return `Toto pole je povinné.`;
    }
    return null;
};

export const psswdMustDiffer = (field1, field2) => {
    if (field1 === field2) {
        return `Nové heslo nesmí být stejné jako původní.`
    }
    return null;
};