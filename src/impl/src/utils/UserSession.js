var UserSession = (() => {
	var auth = false;

	var login = () => {
		console.log('login');
		auth = true;
	};

	var logout = () => {
		console.log('logout');
		auth = false;
	};

	var isAuthenticated = () => {
		return auth;
	};

	return {
		isAuthenticated,
		login,
		logout,
	};
})();

export default UserSession;