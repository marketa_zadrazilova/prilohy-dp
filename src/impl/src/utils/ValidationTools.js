const PROJECT_NAME_MAX_LENGTH = 25;


export function validateLogInForm({email, password}) {
	var validation = {};

	if (email === '') {
		validation['email'] = 'Vyplňte prosím email.';
	}

	if (password === '') {
		validation.password = 'Vyplňte prosím heslo.';
	}

	return validation;
}

export function validateSignUpForm({name, email, password}) {
	var validation = {};

	if (name === '') {
		validation.name = 'Vyplňte prosím jméno.';
	}

	if (email === '') {
		validation.email = 'Vyplňte prosím email.';
	}

	if (password === '') {
		validation.password = 'Vyplňte prosím heslo.';
	}

	return validation;
}

function isEmail(string) {
	return /^[a-z].*@.*\..*$/i.test(string);
}

function hasMinLength(string, length) {
	return string.length >= length;
}

function hasMaxLength(string, length) {
	return string.length <= length;
}


export function validateNewProjectForm(project) {
	var errors = {};

}

export function validateProjectDetails(project) {
	var errors = {};

	if (project.name === '') {
		errors.name = 'Vyplňte prosím název projektu.';
	}

	if (project.type === '') {
		errors.type = 'Vyplňte prosím typ projektu.';
	}

	if (project.descriptionShort) {
		errors.descriptionShort = 'Vyplňte prosím krátký popis projektu.';
	}

	if (project.descriptionLong) {
		errors.descriptionLong = 'Vyplňte prosím detailní popis projektu.';
	}

	return errors;
}

/**
 * Validation rules
 * 
 * Project detail section:
 * - title: isRequired, 
 * - short description:
 */

