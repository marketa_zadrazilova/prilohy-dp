/**
 * Date Utils
 */

export function getFormatedTime() {
	const date = new Date();
	return date.toISOString();
}


export function format(ISOString) {
	const date = new Date(ISOString);

	const time = date.toLocaleTimeString();

	return date.toLocaleDateString() + " v " + time.substring(0, time.length - 3);
}