import ActionTypes from '../actions/ActionTypes';


const initialState = {
	comments: []
};

const commentsReducer = (state = initialState, action) => {
	switch(action.type) {
		case ActionTypes.COMMENTS_FETCH_SUCCESS:
			return {
				comments: [...action.comments]
			};
		default:
			return state;
	}
};

export default commentsReducer;