import { combineReducers } from 'redux';
import commentsReducer from './commentsReducer';


const mainReducer = combineReducers({
	comments: commentsReducer
});

export default mainReducer;