import { connect } from 'react-redux';
import Comments from '../components/comments/Comments';
import { addComment } from '../actions/comments';
import UserSession from '../utils/UserSession';


const mapStateToProps = (state) => {
	return {
		auth: {
			isAuthenticated: UserSession.isAuthenticated(),
			name: 'Josef Holý'
		},
		comments: state.comments.comments
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		onFormSubmitted: (text) => {
			const comment = {
				text
			};

			dispatch(addComment(4, comment));
		}
	};
}

const CommentsContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Comments);

export default CommentsContainer;