import React from 'react';
import { Redirect } from 'react-router-dom';
import SignUpForm from '../pages/SignUpForm';
import UserSession from '../utils/UserSession';
import { validateSignUpForm } from '../utils/ValidationTools';

export default class SignUpContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				name: '',
				email: '',
				password: '',
			},
			redirect: false,
			validation: {},
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleFormSubmitted = this.handleFormSubmitted.bind(this);
	}

	handleInputChange(e) {
		const target = e.target;
		const name = target.name;
		const value = target.value;

		this.setState({
			form: {
				...this.state.form,
				[name]: value
			}
		});
	}

	handleFormSubmitted(e) {
		e.preventDefault();

		const validation = validateSignUpForm(this.state.form);

		if (validation.email || validation.password || validation.name) {
			this.setState({
				validation
			});

			return;
		}

		// form is valid

		UserSession.login();

		this.setState({
			form: {
				name: '',
				email: '',
				password: ''
			},
			redirect: true,
			validation: {},
		});
	}

	render() {
		if (this.state.redirect) {
			return <Redirect to='/'/>
		}

		return (
			<SignUpForm
				form={this.state.form}
				onInputChange={this.handleInputChange}
				onFormSubmitted={this.handleFormSubmitted}
				validation={this.state.validation}
			/>
		);
	}
}