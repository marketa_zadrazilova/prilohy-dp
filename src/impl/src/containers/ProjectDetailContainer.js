import React from 'react';
import ProjectDetail  from '../pages/ProjectDetail';
import * as DataUtils from '../fakeit/DataUtils';


export default class ProjectDetailContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			project: {},
			author: {},
			news: {},
			gallery: {},
			costs: {},
			key: 'details',
		};

		this.handleTabSelect = this.handleTabSelect.bind(this);
	}

	handleTabSelect(key) {
		this.setState({key});
	}

	componentWillMount() {
		const projectId = Number(this.props.match.params.id);
		const project = DataUtils.getProject(projectId);
		const author = DataUtils.getUserById(project.author);
		const news = DataUtils.getNews();
		const gallery = DataUtils.getGallery();
		const costs = project.costs;

		this.setState({
			author,
			project,
			news,
			gallery,
			costs,
		});
	}

	render() {
		return (
			<ProjectDetail
				author={this.state.author} 
				project={this.state.project}
				news={this.state.news}
				gallery={this.state.gallery}
				costs={this.state.costs}
				tabKey={this.state.key}
				onTabSelect={this.handleTabSelect}
			/>
		);
	}
}