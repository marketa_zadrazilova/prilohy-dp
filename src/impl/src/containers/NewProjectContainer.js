import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import NewProject from '../pages/NewProject';
import * as DataUtils from '../fakeit/DataUtils';


const DEFAULT_STATE = {
	activeTab: 1,
	project: {
		name: '',
		categories: [],
		descriptionShort: '',
		descriptionLong: '',
		location: {
			title: '',
			coordinateLat: undefined,
			coordinateLong: undefined,
		},
		costs: [],
		gallery: [],
	},
	costsInput: {
		itemTitle: '',
		itemUnitCost: '',
		itemUnit: '',
		itemUnitCount: '',
	},
	validation: {}
};

class NewProjectContainer extends React.Component {
	constructor(props) {
		super(props);

		const defaultState = props.project ? { ...DEFAULT_STATE, project: {...props.project}} : DEFAULT_STATE;

		console.log('PROJECT', props.project);
		console.log('DEFAULT STATE', defaultState);

		this.state = defaultState;

		this.onPrev = this.onPrev.bind(this);
		this.onNext = this.onNext.bind(this);
		this.handleSelect = this.handleSelect.bind(this);

		this.onItemChange = this.onItemChange.bind(this);
		this.onCategoriesChange = this.onCategoriesChange.bind(this);
		this.handleAddCost = this.handleAddCost.bind(this);
		this.handleRemoveCost = this.handleRemoveCost.bind(this);

		this.handleSubmitProject = this.handleSubmitProject.bind(this);
		this.handleSaveProject = this.handleSaveProject.bind(this);
	}

	componentDidMount() {
		if (this.props.mode === 'UPDATE') {
			console.log('fetch data');
		}
	}

	onItemChange(e) {
		const target = e.target;
		const name = target.name;
		const value = target.value;

		if (name === 'title') {
			this.setState({
				project: {
					...this.state.project,
					location: {
						...this.state.project.location,
						[name]: value,
					}
				}
			});
		} else if (name.startsWith('item')) {
			this.setState({
				costsInput: {
					...this.state.costsInput,
					[name]: value,
				}
			})
		} else {
			this.setState({
				project: {
					...this.state.project,
					[name]: value,
				}
			});
		}
	}

	onCategoriesChange(values) {
		console.log("Values", values);
		this.setState({
			project: {
				...this.state.project,
				categories: values
			}
		});
	}
 
	handleAddCost() {
		//TODO: validate cost
		const newCost = {
			title: this.state.costsInput.itemTitle,
			unitCost: this.state.costsInput.itemUnitCost,
			unitCount: this.state.costsInput.itemUnitCount,
			unit: this.state.costsInput.itemUnit,
		};

		const newCosts = [...this.state.project.costs, newCost];

		this.setState({
			project: {
				...this.state.project,
				costs: newCosts
			},
			costsInput: {
				itemTitle: '',
				itemUnitCost: '',
				itemUnitCount: '',
				itemUnit: ''
			}
		});
	}

	handleRemoveCost(cost) {
		const costs = this.state.project.costs.filter(item => item !== cost);

		this.setState({
			project: {
				...this.state.project,
				costs
			}
		});
	}

	handleSubmitProject() {
		//TODO: validace
		const project = {
			...this.state.project,
			author: 4,
			location: {
				title: this.state.project.location.title,
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			projectState: 'AWAITING'
		}

		const savedProject = DataUtils.saveProject(project);

		this.setState({
			project: savedProject
		});

		alert('Projekt byl úspěšně vytvořen a odeslán ke schválení.');
		//TODO: redirect to my projects
		this.props.history.push(`/autor/projekty/${savedProject.id}`);
	}

	handleSaveProject() {
		const project = Object.assign({}, this.state.project, {
			location: {
				title: this.state.project.location.title,
				coordinateLat: 49.309713,
				coordinateLong: 14.149001
			},
			author: 4,			
			projectState: 'NEW',
		});

		console.log('composed object', project);

		this.setState({
			project: DataUtils.saveProject(project)
		});

		alert('Projekt byl úspěšně uložen.');
	}

	/* TODO - move to another component */
	handleSelect(key) {
		const activeTab = Number(key);

		this.setState({
			activeTab
		});
	}

	onPrev() {
		const newTab = this.state.activeTab - 1;

		this.setState({
			activeTab: (newTab < 1) ? 1 : newTab,
		});
	}

	onNext() {
		const newTab = this.state.activeTab + 1;

		this.setState({
			activeTab: newTab > 5 ? 5 : newTab,
		});
	}

	render() {
		return (
			<NewProject 
				activeTab={this.state.activeTab}
				form={this.state.project}
				onItemChange={this.onItemChange}
				onCategoriesChange={this.onCategoriesChange}
				onPrev={this.onPrev}
				onNext={this.onNext}
				handleSelect={this.handleSelect}
				onSubmitProject={this.handleSubmitProject}
				onSave={this.handleSaveProject}
				onAddCost={this.handleAddCost}
				onRemoveCost={this.handleRemoveCost}
				costsInput={this.state.costsInput}
			/>
		);
	}
}

export default withRouter(NewProjectContainer);

NewProjectContainer.propTypes = {
	mode: PropTypes.string
};