import React from 'react';
import { Redirect } from 'react-router-dom';
import LogInForm from '../pages/LogInForm';
import UserSession from '../utils/UserSession';
import { validateLogInForm } from '../utils/ValidationTools';


class LogInContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				email: '',
				password: '',
			},
			redirect: false,
			validation: {},
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleFormSubmit = this.handleFormSubmit.bind(this);
	}

	handleInputChange(e) {
		const target = e.target;
		const name = target.name;
		const value = target.value;

		this.setState({
			form: {
				...this.state.form,
				[name]: value
			}
		});
	}

	handleFormSubmit(e) {
		e.preventDefault();

		const validation = validateLogInForm(this.state.form);

		if (validation.email || validation.password) {
			this.setState({
				validation
			});

			return;
		}

		// form is valid
		UserSession.login();

		this.setState({
			form: {
				email: '',
				password: ''
			},
			redirect: true,
			validation: {},
		});
	}

	render() {
		//redirect back to the path from which user was redirected
		if (this.state.redirect) {
			const { from } = this.props.location.state || {from : {pathname: '/autor/projekty'}};
			return <Redirect to={from} />
		}

		return (
			<LogInForm 
				form={this.state.form}
				onInputChange={this.handleInputChange}
				onFormSubmit={this.handleFormSubmit}
				validation={this.state.validation}
			/>
		);
	}
}

export default LogInContainer;