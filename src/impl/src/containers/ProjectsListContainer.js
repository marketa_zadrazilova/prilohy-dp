import React from 'react';
import ProjectsList from '../pages/ProjectsList';
import * as DataUtils from '../fakeit/DataUtils';


export default class ProjectsListContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			projects: []
		}
	}

	componentDidMount() {
		const publicProjects = DataUtils.getPublicProjects();
		setTimeout(() => this.setState({projects: publicProjects}), 300);
	}

	render() {
		return <ProjectsList projects={this.state.projects} />;
	}
}