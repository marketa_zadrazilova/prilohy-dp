import React from 'react';
import * as DataUtils from '../fakeit/DataUtils';
import SearchResult from '../pages/SearchResult';


export default class SearchResultContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			query: '',
			projects: [],
		};
	}

	componentDidMount() {
		const query = this.props.location.state.query;
		var projects = DataUtils.getPublicProjects();

		this.setState({
			query: query,
			projects: projects
		});
	}

	render() {
		return (
			<SearchResult 
				projects={this.state.projects}
				query={this.state.query}
			/>
		);
	}
}