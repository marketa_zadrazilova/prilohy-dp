import React from 'react';
import { withRouter } from 'react-router-dom';
import Header from '../components/layout/Header';
import UserSession from '../utils/UserSession';


const HeaderContainer = ({history}) => (
	<Header 
		auth={{
			isAuthenticated: UserSession.isAuthenticated(),
			name: 'Josef Holý',
		}} 
		onLogout={() => {
				UserSession.logout();
				history.push('/');
		}} 
	/>
);

export default withRouter(HeaderContainer);