import React from 'react';

import AddNews from '../components/news/AddNews';
import NewsList from '../components/news/NewsList';

import { getFormatedTime } from '../utils/DateUtils';


export default class ManageNewsContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			news: [],
			form: {
				title: '',
				text: '',
			},
			showForm: false,
		};

		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleAddNews = this.handleAddNews.bind(this);

		this.handleShowForm = this.handleShowForm.bind(this);
		this.handleHideForm = this.handleHideForm.bind(this);
	}

	handleFormChange(e) {
		this.setState({
			form: {
				...this.state.form,
				[e.target.name]: e.target.value,
			}
		})
	}

	handleAddNews() {
		const {
			form 
		} = this.state;

		if (form.title === '' || form.text === '') {
			alert('Nevalidní novinka');
			return;
		}

		const news = {
			title: form.title, 
			text: form.text,
			created_at: getFormatedTime(),
		};

		this.setState({
			news: [...this.state.news, news],
			form: {
				title: '',
				text: '',
			},
			showForm: false,
		});

		alert('Novinka byla úspěšně zveřejněna.');
	}

	handleHideForm() {
		this.setState({showForm: false});
	}

	handleShowForm() {
		this.setState({showForm: true});
	}

	componentDidMount() {
		//load news and set news
	}

	render() {
		return (
			<div>
				<AddNews 
					form={this.state.form}
					onFormChange={this.handleFormChange} 
					onFormSubmit={this.handleAddNews} 
					showModal={this.state.showForm}
					onShowModal={this.handleShowForm}
					onHideModal={this.handleHideForm}
				/>
				<NewsList 
					news={this.state.news} 
				/>
			</div>
		);
	}
}