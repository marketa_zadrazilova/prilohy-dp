import React from 'react';
import MyProfile from '../pages/MyProfile';
import * as DataUtils from '../fakeit/DataUtils';


export default class MyProfileContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			profile: {
				name: '',
				email: '',
				about: '',
			},
			passwords: {
				newPsswd: '',
				oldPsswd: '',
			}
		};

		this.handleProfileChange = this.handleProfileChange.bind(this);
		this.handleUpdateProfile = this.handleUpdateProfile.bind(this);
		this.handlePasswordsChange = this.handlePasswordsChange.bind(this);	
	}

	handleProfileChange(e) {
		this.setState({
			profile: {
				...this.state.profile,
				[e.target.name]: e.target.value
			}
		});
	}

	handleUpdateProfile() {
		alert("Změny ve Vašem profilu byly úspěšně uloženy.");
		DataUtils.updateUser(4, this.state.profile);
	}

	handlePasswordsChange(e) {
		this.setState({
			passwords: {
				...this.state.passwords,
				[e.target.name]: e.target.value
			}
		});
	}

	componentWillMount() {
		const userData = DataUtils.getUserById(4);

		this.setState({profile: userData});
	}

	render() {
		return (
			<MyProfile 
				profile={this.state.profile} 
				onProfileChange={this.handleProfileChange}
				onProfileUpdate={this.handleUpdateProfile}
				passwords={this.state.passwords}
				onPasswordsChanged={this.handlePasswordsChange}
			/>
		);
	}
}