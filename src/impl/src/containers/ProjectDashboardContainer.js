import React from 'react';
import ProjectDashboard from '../pages/ProjectDashboard';
import * as DataUtils from '../fakeit/DataUtils';
																																																								

export default class ProjectDashboardContainer extends React.Component {
	componentWillMount() {
		const projectId = Number(this.props.match.params.id);
		const news = DataUtils.getNews();
		const project = DataUtils.getProject(projectId);
		const costs = project.costs;

		this.setState({
			project,
			news,
			costs
		});
	}
																																																																																																																																																																						
	render() {
		return (
			<ProjectDashboard 
				project={this.state.project} 
				news={this.state.news}
				costs={this.state.costs} 
			/>
		);																																																																																																																																																																				
	}
}