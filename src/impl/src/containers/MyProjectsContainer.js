import React from 'react';
import MyProjects from '../pages/MyProjects';
import * as DataUtils from '../fakeit/DataUtils';


export default class MyProjectsContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			projects: []
		};
	}

	componentWillMount() {
		const myProjects = DataUtils.getMyProjects();
		console.log(myProjects);
		this.setState({projects: myProjects});
	}

	render() {
		return (
			<MyProjects projects={this.state.projects} />
		);
	}
}