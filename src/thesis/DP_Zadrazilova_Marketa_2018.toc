\select@language {czech}
\select@language {czech}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{viii}{section*.1}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}
\contentsline {chapter}{\chapternumberline {1}Motivace a c\IeC {\'\i }le diplomov\IeC {\'e} pr\IeC {\'a}ce}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Projekt CityFunder}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Strategie rozvoje}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Rizika projektu}{4}{subsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.2.1}R1. Nez\IeC {\'a}jem radnic}{5}{subsubsection.1.1.2.1}
\contentsline {subsubsection}{\numberline {1.1.2.2}R2. Legislativn\IeC {\'\i } p\IeC {\v r}ek\IeC {\'a}\IeC {\v z}ka}{5}{subsubsection.1.1.2.2}
\contentsline {subsubsection}{\numberline {1.1.2.3}R3. Nez\IeC {\'a}jem ob\IeC {\v c}an\IeC {\r u} podporovat projekty}{5}{subsubsection.1.1.2.3}
\contentsline {subsubsection}{\numberline {1.1.2.4}R4. Nedostatek n\IeC {\'a}pad\IeC {\r u}}{6}{subsubsection.1.1.2.4}
\contentsline {subsubsection}{\numberline {1.1.2.5}R5. Podvodn\IeC {\'y} projekt}{6}{subsubsection.1.1.2.5}
\contentsline {section}{\numberline {1.2}Vymezen\IeC {\'\i } autorstv\IeC {\'\i }}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}C\IeC {\'\i }le diplomov\IeC {\'e} pr\IeC {\'a}ce}{7}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Anal\IeC {\'y}za existuj\IeC {\'\i }c\IeC {\'\i }ch \IeC {\v r}e\IeC {\v s}en\IeC {\'\i } pro~ob\IeC {\v c}ansk\IeC {\'e} a~komunitn\IeC {\'\i } m\IeC {\v e}stsk\IeC {\'e} projekty}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Participativn\IeC {\'\i } rozpo\IeC {\v c}et}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}Crowdfunding zam\IeC {\v e}\IeC {\v r}en\IeC {\'y} na~ob\IeC {\v c}ansk\IeC {\'e} projekty}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Srovn\IeC {\'a}n\IeC {\'\i } participativn\IeC {\'\i }ho rozpo\IeC {\v c}tu a~ob\IeC {\v c}ansk\IeC {\'e}ho crowdfundingu}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Zhodnocen\IeC {\'\i } re\IeC {\v s}er\IeC {\v s}n\IeC {\'\i } \IeC {\v c}\IeC {\'a}sti}{13}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Pr\IeC {\r u}zkum u\IeC {\v z}ivatel\IeC {\r u} a kontextu}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Dotazn\IeC {\'\i }kov\IeC {\'y} pr\IeC {\r u}zkum}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}V\IeC {\'y}stupy dotazn\IeC {\'\i }ku}{15}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Individu\IeC {\'a}ln\IeC {\'\i } kvalitativn\IeC {\'\i } rozhovory}{16}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}R\IeC {\'a}mcov\IeC {\'e} ot\IeC {\'a}zky}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Stru\IeC {\v c}n\IeC {\'y} report z rozhovor\IeC {\r u}}{17}{subsection.3.2.2}
\contentsline {chapter}{\chapternumberline {4}Specifikace po\IeC {\v z}adavk\IeC {\r u}}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Funk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{21}{section.4.1}
\contentsline {section}{\numberline {4.2}Nefunk\IeC {\v c}n\IeC {\'\i } po\IeC {\v z}adavky}{22}{section.4.2}
\contentsline {chapter}{\chapternumberline {5}N\IeC {\'a}vrh u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}Rozsah n\IeC {\'a}vrhu u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{23}{section.5.1}
\contentsline {section}{\numberline {5.2}\IeC {\v Z}ivotn\IeC {\'\i } cyklus projektu}{23}{section.5.2}
\contentsline {section}{\numberline {5.3}Wireframe model}{28}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Ve\IeC {\v r}ejn\IeC {\'y} detail projektu}{28}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Formul\IeC {\'a}\IeC {\v r} pro vytvo\IeC {\v r}en\IeC {\'\i } projektu}{30}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Spr\IeC {\'a}va projektu}{30}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}V\IeC {\'y}b\IeC {\v e}r kategori\IeC {\'\i } projekt\IeC {\r u} pomoc\IeC {\'\i } metody t\IeC {\v r}\IeC {\'\i }d\IeC {\v e}n\IeC {\'\i } karet}{32}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Popis metody}{34}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}P\IeC {\v r}\IeC {\'\i }prava experimentu}{35}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}V\IeC {\'y}sledky testov\IeC {\'a}n\IeC {\'\i }}{36}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Z\IeC {\'a}v\IeC {\v e}ry z experimentu}{37}{subsection.5.4.4}
\contentsline {chapter}{\chapternumberline {6}Implementace prototypu front-endov\IeC {\'e} \IeC {\v c}\IeC {\'a}sti platformy}{39}{chapter.6}
\contentsline {section}{\numberline {6.1}V\IeC {\'y}b\IeC {\v e}r technologie pro implementaci}{39}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Single page aplikace}{39}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Javascriptov\IeC {\'e} frameworky}{40}{subsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.2.1}React.js}{40}{subsubsection.6.1.2.1}
\contentsline {subsection}{\numberline {6.1.3}CSS frameworky}{41}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Implementace}{41}{section.6.2}
\contentsline {chapter}{\chapternumberline {7}Vyhodnocen\IeC {\'\i } pou\IeC {\v z}itelnosti navr\IeC {\v z}en\IeC {\'e}ho u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{43}{chapter.7}
\contentsline {section}{\numberline {7.1}U\IeC {\v z}ivatelsk\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{43}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Pap\IeC {\'\i }rov\IeC {\'y} prototyp}{43}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Pr\IeC {\r u}b\IeC {\v e}h testov\IeC {\'a}n\IeC {\'\i }}{45}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Sc\IeC {\'e}n\IeC {\'a}\IeC {\v r}e pro testov\IeC {\'a}n\IeC {\'\i }}{45}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}Report z testov\IeC {\'a}n\IeC {\'\i }}{47}{subsection.7.1.4}
\contentsline {subsubsection}{\numberline {7.1.4.1}Stupn\IeC {\v e} hodnocen\IeC {\'\i } z\IeC {\'a}va\IeC {\v z}nosti}{47}{subsubsection.7.1.4.1}
\contentsline {subsubsection}{\numberline {7.1.4.2}Seznam odhalen\IeC {\'y}ch nedostatk\IeC {\r u}}{47}{subsubsection.7.1.4.2}
\contentsline {section}{\numberline {7.2}Zhodnocen\IeC {\'\i } testov\IeC {\'a}n\IeC {\'\i } pou\IeC {\v z}itelnosti}{50}{section.7.2}
\contentsline {chapter}{\chapternumberline {8}Doporu\IeC {\v c}en\IeC {\'\i } pro dal\IeC {\v s}\IeC {\'\i } rozvoj syst\IeC {\'e}mu}{51}{chapter.8}
\contentsline {chapter}{Z{\' a}v{\v e}r}{53}{chapter*.49}
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{55}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Seznam karti\IeC {\v c}ek pro card sorting}{57}{appendix.B}
\contentsline {appendix}{\chapternumberline {C}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{61}{appendix.C}
