V adresáři src se nachází zdrojové kódy implementace a zdrojová forma práce ve formátu LATEX.
V adresáři test se nachází text práce ve formátu PDF a PS.
V adresáři research se nachází výstupy z dotazníkového průzkumu a audio záznamy z kvalitativních rozhovorů.
V adreáři wireframes se nachází obrázky ve formátu PNG znázorňující wireframe model uživatelského rozhraní.
